
#include <Stepper.h>
#include <Servo.h>
#include <math.h>

#define INVERSE_KENEMATICS

const int stepsPerRevolution = 2038;

Stepper st1(stepsPerRevolution, 6, 5);
Stepper st2(stepsPerRevolution, 9, 8);
Stepper st3(stepsPerRevolution, 12, 11);

const int st1EnablePin = 4;
const int st2EnablePin = 7;
const int st3EnablePin = 10;
const int st1Lim = A6;
const int st2Lim = A7;
const int st1DirToZero = 1;
const int st2DirToZero = -1;

const int buzzerPin = 2;
const int gripperPwmPin = 3;

const int analogHalf = 512;

Servo servo1;

int i1 = 0, i2 = 0, i3 = 0, i4 = 0; // number of steps the motor has taken

int angle1 = 90;

const float armLength2 = 80.f * 80.f;
const int stepsPerHalf = 4891 / 2;
const float initAlpha = (59.f + 5.f) / 180.f * PI;
const float initGamma = 5.f / 180.f * PI;
const float anglePerStep = 360.f / 4076;

float x, y, a;
float stepsAlpha, stepsGamma;

void stopAll() {
  digitalWrite(st1EnablePin, LOW);
  digitalWrite(st2EnablePin, HIGH);
  digitalWrite(st3EnablePin, HIGH);
  tone(buzzerPin, 600);
  while(1);
}

bool isAtLimit(int lim) {
  return analogRead(lim) > analogHalf;
}

bool findZero(Stepper& s, int lim, int dirToZero) {
  const int findZeroSpeed = 10;
  long start;
  if (isAtLimit(lim)) {
    start = millis();
    while (isAtLimit(lim) && (millis() - start) < 5000) {
      s.step(-dirToZero);
      delay(findZeroSpeed);
    }
    if (isAtLimit(lim)) return false;
  }
  start = millis();
  while (!isAtLimit(lim) && (millis() - start) < 60000) {
    s.step(dirToZero); 
    delay(findZeroSpeed);
  }
  if (!isAtLimit(lim)) return false;
  start = millis();
  // TODO count kickback
  while (isAtLimit(lim) && (millis() - start) < 5000) {
    s.step(-dirToZero); 
    delay(findZeroSpeed);
  }
  return !isAtLimit(lim);
}

void calibrate() {
  bool success = 
    findZero(st1, st1Lim, st1DirToZero) &&
    findZero(st2, st2Lim, st2DirToZero);
  if (!success) {
    stopAll();
    return;
  }

  stepsAlpha = initAlpha / PI * stepsPerHalf;
  stepsGamma = initGamma / PI * stepsPerHalf;
  float alpha = initAlpha - initGamma;
  float phi = PI / 2 - initGamma - alpha / 2;
  float d = sqrt(2 * armLength2 * (1 - cos(alpha)));
  x = cos(phi) * d; y = sin(phi) * d;
}

template<class Num>
inline Num clamp(Num i, Num mini, Num maxi) {
  return i < mini ? mini : i > maxi ? maxi : i;
}

void readPot(int a, int* dir, int* i) {
  int c = analogRead(a);
  int b = abs(c - analogHalf);
  if (b < 50) {
    *dir = 0;
    *i = 0;
    return 0;
  }
  *dir = c < analogHalf ? -1 : 1;
  *i = 300 / b; 
}

template<class Driver> 
void checkAndStep(Driver& driver, int& timestamp, int dir, int interval) {
  if (dir != 0) {
    if (timestamp >= interval) {
      driver.step(dir);
      timestamp = 0;
    } else {
      timestamp++;
    }
  }  
}

struct ServoDriver {
  Servo& s;
  int& angle;
  void step(int val) {
    angle = clamp(angle + val, 85, 170);
    s.write(angle);        
  }
} sd = {servo1, angle1};

#ifdef INVERSE_KENEMATICS
bool validXY = true;
struct CoordXWrapper {
  void step(int dx) { x = clamp(x + dx, 0.f, 150.f); validXY = false; }
} cxw;
struct CoordYWrapper {
  void step(int dy) { y = clamp(y - dy, 0.f, 120.f); validXY = false; }
} cyw;

void printCoords() {
  Serial.print("C");
  Serial.print(x);
  Serial.print(",");
  Serial.print(y);
  Serial.print(",");
  Serial.print(a);
  Serial.println();  
}

void updateXY() {
  if (validXY) return;

  const int moveSpeed = 5;
  float d2 = x * x + y * y;
  float alpha = acos(1 - d2 / 2 / armLength2);
  float phi = atan2(y, x);
  float gamma = PI / 2 - phi - alpha / 2;
  float alpha_ = (alpha + gamma) / PI * stepsPerHalf;
  float gamma_ = gamma / PI * stepsPerHalf;

  long diff1 = (long)gamma_ - (long)stepsGamma;
  stepsGamma = gamma_;
  long diff2 = (long)alpha_ - (long)stepsAlpha;
  stepsAlpha = alpha_;

  while (diff1 != 0 || diff2 != 0) {
    if (diff1 > 0) {
      st1.step(-st1DirToZero);
      diff1--;
    } else if (diff1 < 0) {
      if (!isAtLimit(st1Lim)) {
        st1.step(st1DirToZero);
      }
      diff1++;
    }
    if (diff2 > 0) {
      st2.step(-st2DirToZero); diff2--;
    } else if (diff2 < 0) {
      if (!isAtLimit(st2Lim)) {
        st2.step(st2DirToZero);
      }
      diff2++;
    }
    delay(moveSpeed);
  }

  printCoords();
  validXY = true;
}
#endif


void setup() {
  Serial.begin(9600);

  const int stepperDefaultSpeed = 15; // rpm
  st1.setSpeed(stepperDefaultSpeed);
  pinMode(st1EnablePin, OUTPUT); digitalWrite(st1EnablePin, HIGH);
  st2.setSpeed(stepperDefaultSpeed);
  pinMode(st2EnablePin, OUTPUT); digitalWrite(st2EnablePin, HIGH);
  st3.setSpeed(stepperDefaultSpeed);
  pinMode(st3EnablePin, OUTPUT); digitalWrite(st3EnablePin, HIGH);

  servo1.attach(gripperPwmPin);
  servo1.write(angle1);

  pinMode(buzzerPin, OUTPUT);
  tone(buzzerPin, 440, 100);
  calibrate();

  tone(buzzerPin, 1000, 300);
  delay(300);
  tone(buzzerPin, 440, 100);
#ifdef INVERSE_KENEMATICS
  printCoords();
  x = 80.f; y = 40.f;
  validXY = false; updateXY();
  tone(buzzerPin, 1000, 100);
  printCoords();
#endif
}

bool handleSerial() {
#ifdef INVERSE_KENEMATICS
  if (!Serial.available()) return false;
  printCoords();
  return true;
#else
  return false;
#endif
}


void loop() {
  if (handleSerial()) return;
  
  int dir0, dir1, dir2, int0, int1, int2;

  readPot(A2, &dir1, &int1);
  readPot(A3, &dir2, &int2);

#ifdef INVERSE_KENEMATICS
  checkAndStep(cxw, i1, dir1, int1);
  checkAndStep(cyw, i2, dir2, int2);
  updateXY();
#else
  if (dir1 <= 0 || !isAtLimit(st1Lim)) {
    checkAndStep(st1, i1, dir1, int1);
  }
  if (dir2 >= 0 || !isAtLimit(st2Lim)) {
    checkAndStep(st2, i2, dir2, int2);
  }
#endif

  readPot(A1, &dir0, &int0);
  if (analogRead(A0) > analogHalf) {
    checkAndStep(st3, i3, -dir0, int0);
#ifdef INVERSE_KENEMATICS
    if (dir0 != 0) {
      a += anglePerStep * dir0;
      printCoords();
    }
#endif    
  } else {
    checkAndStep(sd, i4, dir0, int0);    
  }

  delay(10);
}
