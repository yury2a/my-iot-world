# Controlling steppers for EEZYbotArm MK3

Trying to control 3D model printed from https://www.thingiverse.com/thing:2838859

# Hardware mods

The hardware was redisigned to add limit switches to detect "zero" for two upper steppers. The 28byj-48 steppers themself were hacked to be bipolar and SN754410 + NOT-gates are used to control, also reduces amount of pins required to control that.

A joystick was added to performed control in 2D space by using inverse kinematics.

