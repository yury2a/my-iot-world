#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <EEPROM.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 32 // OLED display height, in pixels

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, 4);

const int switch_pin = A3;
int y;

int store_ref;
int store_write;
int store_count;
int total;
long dsecs;

void setup() {
  //Serial.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);
  y = analogRead(switch_pin);
  digitalWrite(LED_BUILTIN, LOW);

  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  display.setTextSize(1);             // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE);        // Draw white text

  int m = EEPROM.read(0);
  if (m != 17) {
    store_ref = 3 + (analogRead(A6) % (EEPROM.length() - 10));
    store_count = 0;
    total = 0;
    dsecs = 0;
    store_write = 3;
  } else {
    store_ref = (EEPROM.read(1) << 8) | EEPROM.read(2);
    store_count = EEPROM.read(store_ref);
    total = (EEPROM.read(store_ref + 1) << 8) | EEPROM.read(store_ref + 2);
    dsecs = (EEPROM.read(store_ref + 3) << 24) | (EEPROM.read(store_ref + 4) << 16) |
            (EEPROM.read(store_ref + 5) << 8) | EEPROM.read(store_ref + 6);
    store_write = 0;
  }
}

int read_switch() {
  const long b_f = 125;
  const long d_f = 1000;

  int x = analogRead(switch_pin);
  y = (x * b_f + y * (d_f - b_f)) / d_f;
  return y;
}

void drawProgress(int p) {
  const int x = 56;
  const int y = 10;
  const int w = 70;
  const int h = 12;
  display.drawRect(x, y, w, h,SSD1306_WHITE);
  int w0 = w * p / 100;
  display.fillRect(x + 1, y + 1, w0 - 1, h - 2,SSD1306_WHITE);
}

const int PRESS_MIN = 480;
const int PRESS_MAX = 525;

int state = 0;
long press_start = 0;
long press_end = 0;
int progress = 0;

void writeData() {
  if (!store_write) return;
    bool update_mag = (store_write & 2) ? true : false;
    bool update_ref = update_mag;
    store_count++;
    if (store_count == 50) {
      store_ref = 3 + ((store_ref - 3 + 17) % (EEPROM.length() - 10));
      store_count = 0;
      update_ref = true;
    }
    EEPROM.write(store_ref + 3, (dsecs >> 24) & 255);
    EEPROM.write(store_ref + 4, (dsecs >> 16) & 255);
    EEPROM.write(store_ref + 5, (dsecs >> 8) & 255);
    EEPROM.write(store_ref + 6, (dsecs >> 0) & 255);
    EEPROM.write(store_ref + 1, (total >> 8) & 255);
    EEPROM.write(store_ref + 2, (total >> 0) & 255);
    EEPROM.write(store_ref, store_count);
    if (update_ref) {
      EEPROM.write(1, (store_ref >> 8) & 255);
      EEPROM.write(2, (store_ref >> 0) & 255);
    }
    if (update_mag) {
      EEPROM.write(0, 17);      
    }
    store_write = 0;
}

void process(int sensorValue) {
  switch (state) {
    case 0: {
      if (sensorValue < PRESS_MIN) break;
      digitalWrite(LED_BUILTIN, HIGH);
      press_start = millis();
      state = 1;
      break;
    }
    case 1: {
      progress = min(100, (millis() - press_start) * 100 / 2000);
      if (sensorValue >= PRESS_MIN) break;
      digitalWrite(LED_BUILTIN, LOW);
      press_end = millis();
      state = 2;
      break;
    }
    case 2: {
      if (sensorValue >= PRESS_MIN) {
        digitalWrite(LED_BUILTIN, HIGH);
        state = 2;
        break;
      }
      if (millis() - press_end < 30) break;
      dsecs += (press_end - press_start) / 100;
      total++;
      store_write |= 1;
      writeData();
      state = 0;
      break;
    }
  }
}

void loop() {
  int sensorValue = read_switch();
  digitalWrite(LED_BUILTIN, sensorValue < PRESS_MIN ? LOW : HIGH);

  process(sensorValue);
  
  //Serial.println(sensorValue);

  display.clearDisplay();
  display.setCursor(0, 0);
  display.print(total);
  display.setCursor(0, 8);
  display.print(dsecs / 10); display.print("."); display.print(dsecs % 10); display.print("s");
  display.setCursor(0, 24);
  display.print(sensorValue);

  drawProgress(progress);
  
  display.display();

  delay(10); 
}
