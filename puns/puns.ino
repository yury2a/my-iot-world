
#include <LiquidCrystal.h>
#include <EEPROM.h>

#include "data.h"

const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);
const int colsCount = 20;
const int rowsCount = 4;

char buf[7][21];
int lineCount;
int postNumber;

void loadPost() {
  memset(buf, 0, sizeof(buf)); 
  char post[7 * 21];
  strcpy_P(post, (char *)pgm_read_word(&(posts[postNumber])));
  int line = 0, pos = 0;
  for (const char* p = post; *p; p++) {
    if (*p == '\n') {
      buf[line][pos] = 0;
      line++;
      pos = 0;
    } else {
      buf[line][pos++] = *p;
    }
  }
  buf[line][pos] = 0;
  lineCount = line + 1;
}

void displayPost1() {
  int lines = lineCount > rowsCount ? rowsCount - 1 : lineCount;

  lcd.clear();
  for (int i = 0; i < lines; i++) {
    lcd.setCursor(0, i);
    lcd.print(buf[i]);
  }

  if (lineCount > rowsCount) {
    lcd.setCursor(colsCount - 3, rowsCount - 1);
    lcd.print("...");
  }
}

void displayPost2() {
  lcd.clear();
  const int offset = rowsCount - 1;
  for (int i = offset; i < lineCount; i++) {
    lcd.setCursor(0, i - offset);
    lcd.print(buf[i]);
  }
}

const int storageOffset = 32;

void loadPostNumber() {
  postNumber = EEPROM.read(storageOffset) | (EEPROM.read(storageOffset + 1) << 8);
  if (postNumber >= (sizeof posts) / (sizeof posts[0])) {
    postNumber = 0;
  }
}

void updatePostNumber() {
  postNumber++;
  EEPROM.write(storageOffset + 1, postNumber >> 8);
  EEPROM.write(storageOffset, postNumber & 255);
}

void displayVoltage() {
  analogRead(A0); // discard first read
  int val = analogRead(A0);
  int volts10 = (val * 50L) >> 10L;
  char volsBuf[5] = {
    '+', '0' + (volts10 / 10), '.', '0' + (volts10 % 10), 0
  };
  lcd.setCursor(colsCount - 4, rowsCount - 1);
  lcd.print(volsBuf);
}

void setup() {
  lcd.begin(colsCount, rowsCount);
  lcd.noAutoscroll();

  loadPostNumber();
  loadPost();
}

bool showVoltage = true;
const int slideSpeed = 3000;

void loop() {
  displayPost1();
  if (showVoltage) {
    displayVoltage();
    delay(1000);
    displayPost1();
    updatePostNumber();
    showVoltage = false;
    delay(slideSpeed - 1000);
  } else {
    delay(slideSpeed);
  }
  if (lineCount > rowsCount) {
    displayPost2();
    delay(slideSpeed);
  }
}
