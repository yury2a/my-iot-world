// Downloaded from https://raw.githubusercontent.com/dabbers/OPun/main/data.json
// CC-BY-4.0 license
const char post_0[] PROGMEM = "Have you ever tried\nan apple pie made\nwith poison apples?\nIt's to die for!";
const char post_1[] PROGMEM = "If there's 1 thing\nI know I can count\non, it's my hands.";
const char post_2[] PROGMEM = "I never knew the\ngravity of throwing\na ball really high\ninto the air, but\nthen it hit me.";
const char post_3[] PROGMEM = "I tried to catch the\nfog, but I mist.";
const char post_4[] PROGMEM = "What did the\ncolonists wear to\nthe Boston Tea\nParty? Tea-Shirts";
const char post_5[] PROGMEM = "Good thing we had\nwheels during the\nfounding of the USA.\nWe needed all the\nhelp we could get\nwith our revolution.";
const char post_6[] PROGMEM = "If you neuter a cat,\nwill it no longer\nhave fur balls?";
const char post_7[] PROGMEM = "Did you hear about\nthe rapper that went\nbankrupt? I hear he\ndoesn't even have\n50 cent to his name.";
const char post_8[] PROGMEM = "I saw a guy tumbling\ndown a hill once.\nI just couldn't stop\nhim, he was really\non a roll.";
const char post_9[] PROGMEM = "";
const char post_10[] PROGMEM = "Why do people use\npuns to fight?\nBecause they have\na duel meaning.";
const char post_11[] PROGMEM = "Do you look down on\npuns because they\nwork on another\nlevel?";
const char post_12[] PROGMEM = "I used to love puns\nas a kid, but then\nsomeone told me puns\nare bad. I guess,\nthat was the day I\nbecame a groan up.";
const char post_13[] PROGMEM = "Why doesn't a person\nwho builds computers\nmake a good parent?\nBecause he makes his\nown motherbored";
const char post_14[] PROGMEM = "There's no 'i' in\ndenial.";
const char post_15[] PROGMEM = "Hard boiled eggs\naren't all that\nthey're cracked up\nto be. They don't\nyolk around!";
const char post_16[] PROGMEM = "What do you call a\nthief who stole too\nmuch gold? Au-full.";
const char post_17[] PROGMEM = "Did you know that\nthe wheel used to\nbe square? Then they\nstarted cutting\ncorners on\nproduction.";
const char post_18[] PROGMEM = "I had a friend who\nwould eat clocks for\nlunch. Eating with\nhim was time\nconsuming.";
const char post_19[] PROGMEM = "Does the name Pavlov\nring a bell?";
const char post_20[] PROGMEM = "Did you hear about\nthe Spanish Train\nkiller? He had\nlocomotives.";
const char post_21[] PROGMEM = "When your jokes are\ntoo good, and people\nsentence you to 2\nyears in a\npunitentiary.";
const char post_22[] PROGMEM = "When you add too\nmuch salt to your\nfood, your taste\nbuds become\nassalted.";
const char post_23[] PROGMEM = "Whiteboards are\nremarkable!";
const char post_24[] PROGMEM = "Shovels were a\nground breaking\ninvention!";
const char post_25[] PROGMEM = "Cemeteries are\nreally popular right\nnow. People are\ndying to get into\nthem.";
const char post_26[] PROGMEM = "Taking the elevator\nreally has its ups\nand downs.";
const char post_27[] PROGMEM = "The scarecrow was\npromoted, because\nhe was outstanding\nin his field.";
const char post_28[] PROGMEM = "The movie The\nLongest Yard, really\nhas its pros and\ncons.";
const char post_29[] PROGMEM = "It's handy, knowing\nsign language.";
const char post_30[] PROGMEM = "The cashews were so\nsalty, but they are\nsodium good.";
const char post_31[] PROGMEM = "Mermaids wear\nSea-shells because\nb-shells are too\nsmall.";
const char post_32[] PROGMEM = "I tried eating my\nbirthday dessert,\nbut I got it all\nover me. I was\ncaked.";
const char post_33[] PROGMEM = "When things get\nturned on in the\nshower, she gets\nwet.";
const char post_34[] PROGMEM = "I named my iPod the\ntitanic, and now\nit's syncing.";
const char post_35[] PROGMEM = "Do you know why\nthere's no gambling\nin Africa? Because\nthere's too many\ncheetahs.";
const char post_36[] PROGMEM = "The invention of the\nbroom really swept\nthe world.";
const char post_37[] PROGMEM = "Writing with a dull\npencil is pointless.";
const char post_38[] PROGMEM = "How do feet smell\nif they don't have\na nose?";
const char post_39[] PROGMEM = "What sea life\ndoesn't help others?\nI'll give you a\nhint, they're\ncrabby, and they're\nshellfish.";
const char post_40[] PROGMEM = "I don't have any\nchange on me, but\nI do have a dollar\nbill. It's noncents.";
const char post_41[] PROGMEM = "What did the Sober\nghost say to his\nfriends at the\nparty? No Boo's for\nme thanks!";
const char post_42[] PROGMEM = "I got canned from\nthe orange juice\nfactory because I\nwouldn't\nconcentrate.";
const char post_43[] PROGMEM = "What do you call a\npile of cats? A\nmeowton.";
const char post_44[] PROGMEM = "Why doesn't anyone\nlike jokes about\nGerman sausages?\nThey're the Wurst.";
const char post_45[] PROGMEM = "The past, present,\nand future all walk\ninto a bar. It was\ntense.";
const char post_46[] PROGMEM = "Why can't you run\nin camp grounds? You\ncan only ran. It's\npast tents.";
const char post_47[] PROGMEM = "What's a welder's\nfavorite music genre\nto listen to?\nHeavy metal.";
const char post_48[] PROGMEM = "Snow melting in the\nsummer is Mother\nNature liquidating\nher assets.";
const char post_49[] PROGMEM = "Having phone sex\nmight result in\ngetting hearing\naids.";
const char post_50[] PROGMEM = "Picture of snakes\non a 3D math plane.";
const char post_51[] PROGMEM = "I ate a pencil once.\nI digested that\nsucker the old\nnatural way. Yes,\nit was in fact a\nnumber 2 pencil.";
const char post_52[] PROGMEM = "Picture of 2 peppers\ndressed up in winter\nattire. Captioned:\n\"Chili Peppers\".";
const char post_53[] PROGMEM = "A picture of 4 figs\ncaptioned Fig. 1-4,\nwith a title of\n\"Figs: A Diagram\"";
const char post_54[] PROGMEM = "Did you know that\ndiarrhea is\nhereditary? It runs\nin your genes.";
const char post_55[] PROGMEM = "I ate four cans of\nalphabet soup\nyesterday. Then I\nhad probably the\nbiggest vowel\nmovement ever.";
const char post_56[] PROGMEM = "A girl's cat\nkidnapped her. It\njust whisker away.";
const char post_57[] PROGMEM = "The crowd was\nconfused by what the\ncat was doing. The\ncat then pawsed to\nexplain it.";
const char post_58[] PROGMEM = "The animal meowed\nfor hours on end.\nIt cat talk any\nother way.";
const char post_59[] PROGMEM = "The cat didn't feel\nwell after falling\nfrom the window.\nShe's feline down.";
const char post_60[] PROGMEM = "The cat made me sign\nthe contract in\nblood. The proof is\nin the second claws!\nShould have read the\ntail end before\nhand.";
const char post_61[] PROGMEM = "We shaved the cat\nyesterday. He's\nfur-ious.";
const char post_62[] PROGMEM = "I escaped Iraq the\nonly way I knew how,\nIran.";
const char post_63[] PROGMEM = "I got fired from the\ncalendar factory.\nAll I did was take\na day off.";
const char post_64[] PROGMEM = "What happened when\nthe police found a\npeep hole into a\nnudist camp? They\nlooked into it.";
const char post_65[] PROGMEM = "I once did a\ntheatrical\nperformance about\npuns. It was a play\non words.";
const char post_66[] PROGMEM = "How do you fix a\ntrain that can't\nhear? With an\nEnginEAR";
const char post_67[] PROGMEM = "Did. Did. Did. Did.\nDid. Did. Did. Did.\nDid. Did.\nNo pun intendid.";
const char post_68[] PROGMEM = "I tried to tell a\nseries of jokes to\nmy friend to see if\nI could get a\nreaction.\nNo pun in ten did.";
const char post_69[] PROGMEM = "A neutron goes into\na bar and asks the\nbartender, \"How much\nfor a beer?\" The\nbartender replies,\n\"For you, no\ncharge.\"";
const char post_70[] PROGMEM = "I have a large\ncollection of candy\ncanes. They're all\nin mint condition.";
const char post_71[] PROGMEM = "Being number 1 is\nodd.";
const char post_72[] PROGMEM = "I only give away\ndead batteries; free\nof charge that is.";
const char post_73[] PROGMEM = "My calculator is\nreally something I\ncan count on.";
const char post_74[] PROGMEM = "The telemarketer\nrespected peoples'\nprivacy by putting\nhis name on the neck\nof his shirt. This\nway he'd have a\ncollar id.";
const char post_75[] PROGMEM = "Log backups are\ngenerally a good\nthing to have unless\nit's in the toilet.";
const char post_76[] PROGMEM = "The best thing about\nBooleans is that\neven if you're\nwrong, you're only\noff by a bit.";
const char post_77[] PROGMEM = "The best part about\nbeing a Web Browser\nis all the free\ncookies.";
const char post_78[] PROGMEM = "Do you cough so much\nthat your coworkers\nwon't let you cough\nat work? Need a\nplace to cough in?\nBuy a coffin!";
const char post_79[] PROGMEM = "They say that the\naverage doesn't\nalways represent the\ndata accurately. I\nguess it just\ndoesn't mean much.";
const char post_80[] PROGMEM = "Why are Assembly\nprogrammers always\nwet? They work below\nC level.";
const char post_81[] PROGMEM = "Did you hear about\nthe person who left\nthe party early by\nthrowing a package\nof hay at people?\nI guess you could\nsay they baled.";
const char post_82[] PROGMEM = "I knew a guy who's\nfoot was a mile\nlong. Trimming his\ntoenails was a big\nfeet.";
const char post_83[] PROGMEM = "I knew a guy who\nlost everything\nbelow his left\nankle. He was 1 foot\nshorter from then\non.";
const char post_84[] PROGMEM = "If two pigs start\ndirty dancing, is\nit called pork\ngrind?";
const char post_85[] PROGMEM = "If a large garbage\ncan sinks in an\nocean, attempting\nto retrieve it would\nbe considered\ndumpster diving.";
const char post_86[] PROGMEM = "The duck added the\ndrinks to his bill.";
const char post_87[] PROGMEM = "Being murdered is\na deadly act.";
const char post_88[] PROGMEM = "Why did C++ reject\nC's proposal for\ngoing on a date? C\nhas no class.";
const char post_89[] PROGMEM = "Why do rabbits like\ndrinking beer? It\ngives them hops.";
const char post_90[] PROGMEM = "I used to create\nshirts and pants\nfrom fashion\nmagazines. I had\nmany articles of\nclothing.";
const char post_91[] PROGMEM = "The guests at the\nwedding started\ncrying when the cake\ncame out because it\nwas made of tiers.";
const char post_92[] PROGMEM = "I tried using\nchicken soup broth\nin a joke once. It\nwas a laughingstock.";
const char post_93[] PROGMEM = "Why did Pumba like\nSimba so much? Simba\nwas his mane man.";
const char post_94[] PROGMEM = "You think Geppetto\ncan tell if\nPinocchio is lying?\nOh, he nose.";
const char post_95[] PROGMEM = "Why does Simba never\norder delivery? It's\neasier to eat\nwhatever is lion\naround.";
const char post_96[] PROGMEM = "What's The Lion King\nfull of? Simba-lism.";
const char post_97[] PROGMEM = "What does Sully\nwrite with?\nMonster's Ink.";
const char post_98[] PROGMEM = "What is Simba's\nfavorite musical\ngenre? Pride rock.";
const char post_99[] PROGMEM = "Who was Jasmine\nattracted to?\nA-lad-in trouble.";
const char post_100[] PROGMEM = "How can the Little\nMermaid avoid all\nher problems under\nthe sea? Go aerial.";
const char post_101[] PROGMEM = "What did Ariel's\nfather say when he\nwas convincing\npeople to do stuff?\nTry-tons.";
const char post_102[] PROGMEM = "What do you say to\nsomeone trying to\nenter the Beast's\ncastle? Ring the\ndoor,\nBelle.";
const char post_103[] PROGMEM = "\"Walt, which part\nof your leg is\nhurting?\" Walt\npoints to left\nkneecap \"Dis knee.\"";
const char post_104[] PROGMEM = "My computer has an\ninterstellar pub,\nit's a space bar.";
const char post_105[] PROGMEM = "\"Mulan! How many\nenemies are on that\nridge?\"\n\"I'm not sure...\nabout a hun-dred?\"";
const char post_106[] PROGMEM = "Does drinking fresh\ntea come at a steep\nprice?";
const char post_107[] PROGMEM = "The wise guy thought\nhe'd fix things by\nturning on the lamp\nwithout a bulb. It\nwasn't very bright.";
const char post_108[] PROGMEM = "I had a friend who\nadjusted bones for\na living in Egypt.\nHe was a\nCairo-practor.";
const char post_109[] PROGMEM = "You allergic to\ncorn? No? Good I've\ngot some corny jokes\nfor you.";
const char post_110[] PROGMEM = "I tried to\nunderstand why the\nguy was so large,\nbut he was ambiguous";
const char post_111[] PROGMEM = "Someone asked me a\nquestion about\ncutting down trees,\nbut I was stumped.\nSo I\nmade up an answer\non a limb.";
const char post_112[] PROGMEM = "I didn't think\nthey'd mind that I\novercooked their\ntortilla, but they\ntold me we needed\nto taco bout it.";
const char post_113[] PROGMEM = "I really wanted a\nrare steak, but the\nwaiter said he\ncouldn't find a\nsteak made of\nunicorn.";
const char post_114[] PROGMEM = "For real though,\nwith out steak, I'd\nhave a cow.";
const char post_115[] PROGMEM = "After some trouble,\nI managed to barter\nfor some raw metal.\nIt was an ore deal.";
const char post_116[] PROGMEM = "What is blue and\ndoesn't weigh very\nmuch? Light blue.";
const char post_117[] PROGMEM = "What's brown and\nsticky? A stick.";
const char post_118[] PROGMEM = "What has four wheels\nand flies? A garbage\ntruck.";
const char post_119[] PROGMEM = "What do you call a\nwaffle lying on a\nbeach in California?\nSandy eggo.";
const char post_120[] PROGMEM = "If you put root beer\nin a square glass,\ndoes it become beer?";
const char post_121[] PROGMEM = "My friend tried\nsmoking my todo\nlist. He was high\non my priorities.";
const char post_122[] PROGMEM = "Smoking weed is a\njoint effort.";
const char post_123[] PROGMEM = "Where does the dog\ngo when it loses its\ntail? The retail\nstore.";
const char post_124[] PROGMEM = "How do dogs travel?\nBy waggin'.";
const char post_125[] PROGMEM = "When does the clock\nwin? When it's one";
const char post_126[] PROGMEM = "I put mint in my hot\nwater. It's minty.";
const char post_127[] PROGMEM = "If you hand someone\nmice as an accolade\nyou are giving them\npreys.";
const char post_128[] PROGMEM = "Did you know groups\nof fish have school\neveryday of the\nweek?";
const char post_129[] PROGMEM = "I used a large\nnoodle costume to\npose as someone\nelse. Everyone knew\nI was impasta.";
const char post_130[] PROGMEM = "I thought I knew\nwhat type of pasta\nI wanted, but\nthere's just so many\npastabilities";
const char post_131[] PROGMEM = "Where do pencils go\non vacation?\nPencilvania.";
const char post_132[] PROGMEM = "Can you imagine how\nmuch deeper the\nocean would be if\nit weren't for\nsponges?";
const char post_133[] PROGMEM = "The inventor of the\nknock knock joke\nreceived a\nno-bell-prize.";
const char post_134[] PROGMEM = "Where would you go,\nif you were thirsty\nfor a carbonated\ndrink, but not that\nthirsty? Minisoda";
const char post_135[] PROGMEM = "Image: Picture of\na Koala with the\ncaption: \"What do\nyou mean I'm not a\nbear, I\nhave all of the\nkoalifications\"";
const char post_136[] PROGMEM = "That person is\nbooking it for the\nlibrary!";
const char post_137[] PROGMEM = "Friend: \"Orion's\nBelt is a big waist\nof stars.\"\nMe: \"Terrible joke.\nOnly 3 stars\".";
const char post_138[] PROGMEM = "To argue against a\nfact, I used a ruler\nin my kitchen as a\ncounter measure.";
const char post_139[] PROGMEM = "I started a business\nbuilding boats.\nSails are through\nthe roof!";
const char post_140[] PROGMEM = "If you wear cowboy\nclothes on a farm,\nare you ranch\ndressing?";
const char post_141[] PROGMEM = "They say that no two\npeople see colors\nthe same way. Is\ncolor a pigment of\nyour imagination?";
const char post_142[] PROGMEM = "Have you ever tried\ncooking your meat\nover a nice, hot\nlava pit? I lava\ngood\nBBQ.";
const char post_143[] PROGMEM = "When coral gets\nstressed out, they\ndie. Their most\nstressing topic?\nCurrent events,\nthough it comes in\nwaves.";
const char post_144[] PROGMEM = "The guy who had\nevery lamp in his\nhouse stolen was\ndelighted when it\nhappened.";
const char post_145[] PROGMEM = "Don't trust atoms,\nthey make up\neverything.";
const char post_146[] PROGMEM = "People told me that\nI'm addicted to\ndrinking break\nfluid. I disagree.\nI can stop any time.";
const char post_147[] PROGMEM = "Do I prefer boxers\nor briefs? Depends.";
const char post_148[] PROGMEM = "My snail was too\nslow, so I took the\nshell off to make\nhim faster. All it\ndid was make him\nsluggish.";
const char post_149[] PROGMEM = "I saw a pirate with\na steering wheel on\nhis pants and I\nbrought it to his\nattention. He simply\nresponded \"Aye, it's\ndriving me nuts!\"";
const char *const posts[] PROGMEM = {
  post_0,
  post_1,
  post_2,
  post_3,
  post_4,
  post_5,
  post_6,
  post_7,
  post_8,
  post_9,
  post_10,
  post_11,
  post_12,
  post_13,
  post_14,
  post_15,
  post_16,
  post_17,
  post_18,
  post_19,
  post_20,
  post_21,
  post_22,
  post_23,
  post_24,
  post_25,
  post_26,
  post_27,
  post_28,
  post_29,
  post_30,
  post_31,
  post_32,
  post_33,
  post_34,
  post_35,
  post_36,
  post_37,
  post_38,
  post_39,
  post_40,
  post_41,
  post_42,
  post_43,
  post_44,
  post_45,
  post_46,
  post_47,
  post_48,
  post_49,
  post_50,
  post_51,
  post_52,
  post_53,
  post_54,
  post_55,
  post_56,
  post_57,
  post_58,
  post_59,
  post_60,
  post_61,
  post_62,
  post_63,
  post_64,
  post_65,
  post_66,
  post_67,
  post_68,
  post_69,
  post_70,
  post_71,
  post_72,
  post_73,
  post_74,
  post_75,
  post_76,
  post_77,
  post_78,
  post_79,
  post_80,
  post_81,
  post_82,
  post_83,
  post_84,
  post_85,
  post_86,
  post_87,
  post_88,
  post_89,
  post_90,
  post_91,
  post_92,
  post_93,
  post_94,
  post_95,
  post_96,
  post_97,
  post_98,
  post_99,
  post_100,
  post_101,
  post_102,
  post_103,
  post_104,
  post_105,
  post_106,
  post_107,
  post_108,
  post_109,
  post_110,
  post_111,
  post_112,
  post_113,
  post_114,
  post_115,
  post_116,
  post_117,
  post_118,
  post_119,
  post_120,
  post_121,
  post_122,
  post_123,
  post_124,
  post_125,
  post_126,
  post_127,
  post_128,
  post_129,
  post_130,
  post_131,
  post_132,
  post_133,
  post_134,
  post_135,
  post_136,
  post_137,
  post_138,
  post_139,
  post_140,
  post_141,
  post_142,
  post_143,
  post_144,
  post_145,
  post_146,
  post_147,
  post_148,
  post_149,
};
