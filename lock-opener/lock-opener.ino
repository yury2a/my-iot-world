#include <CheapStepper.h>
#include <TM1637Display.h>

const int START_BUTTON_PIN = 8;
const int DISPLAY_DATA = 2;
const int DISPLAY_CLK = 3;

const int DIR_SER = 5;
const int DIR_CLK = 4;
const int DIR_LATCH = 6;
const int DIR_EN = 7;
const int PWM_PIN = 9;

const int STEPPER1 = 10;
const int STEPPER2 = 11;
const int STEPPER3 = 12;
const int STEPPER4 = 13;

TM1637Display tm(DISPLAY_CLK,DISPLAY_DATA);
const uint8_t SEGS[] = {
  0, // empty
  0, // empty
  0, // empty
  0, // empty
  SEG_A | SEG_B | SEG_F | SEG_G,   // ^o
  SEG_C | SEG_D | SEG_E | SEG_G,    // _o
  SEG_G,
  SEG_G,
  SEG_G,
  SEG_G,
  SEG_A | SEG_D, // ^_
};


CheapStepper stepper(STEPPER1, STEPPER2, STEPPER3, STEPPER4);
const int TOTAL_STEPS = 4096;

void pulseActuator(bool extend, int dur) {
  shiftOut(DIR_SER, DIR_CLK, MSBFIRST, extend ? 6 : 24);
  digitalWrite(DIR_LATCH, HIGH);
  digitalWrite(DIR_LATCH, LOW);

  digitalWrite(PWM_PIN, HIGH);
  delay(dur);
  digitalWrite(PWM_PIN, LOW);    
}

void rotateCircle(bool moveClockwise) {
  stepper.move(moveClockwise, TOTAL_STEPS);
}

void dial(int num, bool moveClockwise) {
  int k = TOTAL_STEPS * (long)num / 40;
  stepper.moveTo(moveClockwise, k);
}

void offStepper() {
  digitalWrite(STEPPER1, LOW);
  digitalWrite(STEPPER2, LOW);
  digitalWrite(STEPPER3, LOW);
  digitalWrite(STEPPER4, LOW);
}

bool isOpened() {
  return analogRead(A2) > 500;
}

void setup() {
  pinMode(START_BUTTON_PIN, INPUT_PULLUP);
  pinMode(DIR_SER, OUTPUT);
  pinMode(DIR_CLK, OUTPUT);
  pinMode(DIR_LATCH, OUTPUT);
  pinMode(DIR_EN, OUTPUT);
  pinMode(PWM_PIN, OUTPUT);

  digitalWrite(DIR_CLK, LOW);
  digitalWrite(DIR_LATCH, LOW);
  digitalWrite(DIR_EN, LOW);
  digitalWrite(PWM_PIN, LOW);

  tm.setBrightness(0x0f);
  tm.setSegments(SEGS + 6, 4, 0);
  
  stepper.setRpm(12);
}

typedef void (*PeriodicFn)();

void initialPeriodic();
void attemptPeriodic();
void donePeriodic();

void try1Periodic();
void try2Periodic();
void try3Periodic();

PeriodicFn current = initialPeriodic;
PeriodicFn attemptCurrent = try2Periodic;

int digit1, digit2, lastDigit2, digit3, lastDigit3;
bool success;

void initialPeriodic() {
  if (isOpened()) {
    tm.setSegments(SEGS + 10, 1, 2);
    return;
  }
  tm.setSegments(SEGS + 6, 1, 2);

  if (digitalRead(START_BUTTON_PIN) == HIGH) {
    return;
  }

  tm.setSegments(SEGS + 4, 2, 2);
  rotateCircle(false);
  tm.setSegments(SEGS + 4, 2, 0);
  rotateCircle(false);

  // TODO implement trying of digit1
  digit1 = 4;
  tm.setSegments(SEGS + 0, 2, 0);
  tm.showNumberDec(digit1, true, 2, 2);
  dial(digit1, false);
  delay(500);
  tm.setSegments(SEGS + 4, 2, 0);
  rotateCircle(true);
  
  current = attemptPeriodic;
  attemptCurrent = try2Periodic;
  digit2 = 22; lastDigit2 = 26;
  success = false;
}

const int PAD = 3;

void try1Periodic() {
}

void try2Periodic() {
  tm.showNumberDec(digit2, true, 2, 0);
  tm.setSegments(SEGS + 0, 2, 2);
  
  dial(digit2, true);
  delay(500);

  digit3 = (digit2 + 40 - PAD) % 40;
  lastDigit3 = (digit2 + PAD) % 40;
  attemptCurrent = try3Periodic;
}

void try3Periodic() {
  tm.showNumberDec(digit3, true, 2, 2);
  dial(digit3, false);  
  delay(50);
  pulseActuator(false, 200);
  delay(50);

  if (isOpened()) {
    current = donePeriodic;
    offStepper();
    success = true;
    return true;
  }

  pulseActuator(true, 50);
  delay(200);

  if (digit3 == lastDigit3) {
    if (digit2 == lastDigit2) {
      // TODO switch digit1
      current = donePeriodic;
      offStepper();
      return;    
    }
 
    digit2 = (digit2 + 1) % 40;
    attemptCurrent = try2Periodic;
    return;
  }

  digit3 = (digit3 + 39) % 40;
}

void attemptPeriodic() {
  attemptCurrent();
}

void donePeriodic() {
  tm.setSegments(SEGS + 0, 2, 0);
  tm.showNumberDec(digit1, true, 2, 2);
  delay(2000);  
  tm.showNumberDec(digit2, true, 2, 0);
  tm.showNumberDec(digit3, true, 2, 2);
  delay(3000);
  if (!isOpened()) {
    tm.setSegments(SEGS + 6, 4, 0);
    current = initialPeriodic;
  }
}


void loop() {
  current();
}
