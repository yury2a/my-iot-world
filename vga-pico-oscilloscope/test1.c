#include "hardware/structs/systick.h"
#include "hardware/clocks.h"
#include "hardware/adc.h"
#include "hardware/dma.h"
#include "pico/stdlib.h"
#include "pico/multicore.h"

#define CAPTURE_CHANNEL 0
#define CAPTURE_DEPTH 1000

uint8_t capture_buf[CAPTURE_DEPTH];

void core1_entry()
{
  // Code taken from pico-examples/adc/dma_capture/dma_capture.c
  adc_init();
  adc_irq_set_enabled(false);
  adc_gpio_init(26 + CAPTURE_CHANNEL);
  adc_select_input(CAPTURE_CHANNEL);
  adc_fifo_setup(true, true, 1, false, true);
  adc_set_clkdiv(0);

  sleep_ms(1000);
  uint dma_chan = 10; // or dma_claim_unused_channel(true);

  while (1)
  {
    dma_channel_config cfg = dma_channel_get_default_config(dma_chan);

    channel_config_set_transfer_data_size(&cfg, DMA_SIZE_8);
    channel_config_set_read_increment(&cfg, false);
    channel_config_set_write_increment(&cfg, true);

    channel_config_set_dreq(&cfg, DREQ_ADC);

    dma_channel_configure(dma_chan, &cfg,
                          capture_buf,   // dst
                          &adc_hw->fifo, // src
                          CAPTURE_DEPTH, // transfer count
                          true           // start immediately
    );

    adc_run(true);
    dma_channel_wait_for_finish_blocking(dma_chan);
    adc_run(false);
  }
}

#define GET_SYSTICK systick_hw->cvr
const long SYSTICK_MAX = 0x00FFFFFF;
const long SYSTICK_NEG_BIT = 0x00800000;

int main()
{
  set_sys_clock_khz(126000, true);
  systick_hw->csr = 0x5;
  systick_hw->rvr = SYSTICK_MAX;

  multicore_launch_core1(core1_entry);

  gpio_init(3); // horizontal sync
  gpio_set_dir(3, GPIO_OUT);
  gpio_init(4); // vertical sync (inverted)
  gpio_set_dir(4, GPIO_OUT);
  gpio_init(5); // a color
  gpio_set_dir(5, GPIO_OUT);
  gpio_put(3, 0);
  gpio_put(4, 1);
  gpio_put(5, 0);

  while (true)
  {
    for (uint q = 0; q < 525; q++)
    {
      if (q < 480)
      {
        // data lines
        long end = GET_SYSTICK - (640 * 5);
        uint y = q;
        uint8_t *p = capture_buf;
        uint last = p[0];
        uint last_mid = last;
        while (((end - GET_SYSTICK) & SYSTICK_NEG_BIT))
        {
          uint next = *(++p);
          uint mid = (last + next) >> 1;
          if (last_mid > mid)
            gpio_put(5, mid <= y && y < last_mid);
          else
            gpio_put(5, last_mid <= y && y <= mid);
          last = next;
          last_mid = mid;
        }
        gpio_put(5, 0);
      }
      else
      {
        // sync lines
        long end = GET_SYSTICK - (640 * 5);
        gpio_put(5, 0);
        gpio_put(4, (q >> 1) == (490 >> 1) ? 0 : 1);
        while (((end - GET_SYSTICK) & SYSTICK_NEG_BIT))
          ;
      }

      // very short front porch 16 pixels (minus some)
      __asm volatile("nop\nnop\nnop\nnop\nnop\n");
      __asm volatile("nop\nnop\nnop\nnop\nnop\n");
      __asm volatile("nop\nnop\nnop\nnop\nnop\n");
      __asm volatile("nop\nnop\nnop\nnop\nnop\n");
      __asm volatile("nop\nnop\nnop\nnop\nnop\n");
      __asm volatile("nop\nnop\nnop\nnop\nnop\n");
      __asm volatile("nop\nnop\nnop\nnop\nnop\n");
      __asm volatile("nop\nnop\nnop\nnop\nnop\n");
      __asm volatile("nop\nnop\nnop\nnop\nnop\n");
      __asm volatile("nop\nnop\nnop\nnop\nnop\n");
      __asm volatile("nop\nnop\nnop\nnop\nnop\n");
      __asm volatile("nop\nnop\nnop\nnop\nnop\n");
      // __asm volatile ("nop\nnop\nnop\nnop\nnop\n");
      // __asm volatile ("nop\nnop\nnop\nnop\nnop\n");
      // __asm volatile ("nop\nnop\nnop\nnop\nnop\n");
      // __asm volatile ("nop\nnop\nnop\nnop\nnop\n");

      gpio_xor_mask(1 << 3);
      uint j = 96; // pixels
      do
      {
        j -= 1;
        __asm volatile("nop\n");
      } while (j > 0);
      gpio_xor_mask(1 << 3);
      // back porch 48 pixels (minus some)
      uint i = 48 - 2;
      do
      {
        i -= 1;
        __asm volatile("nop\n");
      } while (i > 0);
    }
  }
}
