#include <WiFi.h>
#include <PubSubClient.h>
#include "my.h"

const long SEND_DATA_TIMEOUT = 30000;

WiFiClient wifiClient;
PubSubClient mqttClient(wifiClient);

void sendData(String &data) {
  long end = millis() + SEND_DATA_TIMEOUT;

  WiFi.mode(WIFI_STA);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(WiFi.status());
    delay(500);
    if (end < millis()) {
      WiFi.disconnect();
      return;
    }
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  // Loop until we're reconnected
  while (!mqttClient.connected()) {
    if (end < millis()) {
      WiFi.disconnect();
      return;
    }
    mqttClient.setServer(MQTT_SERVER, MQTT_SERVER_PORT);
    mqttClient.setBufferSize(1000);
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (mqttClient.connect("anemomtr01", MQTT_USER, MQTT_PASSWORD)) {
      Serial.println("connected");
      break;
    } else {
      Serial.print("failed, rc=");
      Serial.print(mqttClient.state());
      Serial.println(" try again in 5 seconds");
      // Wait 2 seconds before retrying
      delay(2000);
    }
  }

  // Once connected, publish an announcement...
  String topic = "local/anemometer/";
  topic += WiFi.macAddress();
  mqttClient.publish(topic.c_str(), data.c_str());
  mqttClient.disconnect();
  WiFi.disconnect();
}
