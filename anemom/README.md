# Simple Anemometer

Sends MQTT message with speed of the anemometer.

Based on ESP32. Using 74HC393 to count rotations.
Most of the time ESP32 is a asleep.

## Network credentials

Copy/rename "my.h.template" into "my.h" and change the names and passwords.
