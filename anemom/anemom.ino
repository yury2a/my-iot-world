#include "netutils.h"
#include <sys/time.h>
#include <ArduinoJson.h>

#include "soc/soc.h"
#include "soc/rtc_cntl_reg.h"

const float BAT_COEF = 2.2f * 3.3f / 4095.0f;
const float RPM_TO_SPEED2 = 0.019f;
const float RPM_TO_SPEED = 0.12f;

int pins[] = {4, 16, 17, 5, 25, 26, 27, 14};

#define uS_TO_S_FACTOR 1000000
#define TIME_TO_SLEEP 3
const int FRAMES_NUM = 12;


void print_wakeup_reason(esp_sleep_wakeup_cause_t wakeup_reason) {
  switch(wakeup_reason)
  {
    case ESP_SLEEP_WAKEUP_EXT0 : Serial.println("Wakeup caused by external signal using RTC_IO"); break;
    case ESP_SLEEP_WAKEUP_EXT1 : Serial.println("Wakeup caused by external signal using RTC_CNTL"); break;
    case ESP_SLEEP_WAKEUP_TIMER : Serial.println("Wakeup caused by timer"); break;
    case ESP_SLEEP_WAKEUP_TOUCHPAD : Serial.println("Wakeup caused by touchpad"); break;
    case ESP_SLEEP_WAKEUP_ULP : Serial.println("Wakeup caused by ULP program"); break;
    default : Serial.printf("Wakeup was not caused by deep sleep: %d\n",wakeup_reason); break;
  }  
}

struct Frame {
  time_t seconds;
  uint8_t counter;
};

// Keep frame 0 populated.
RTC_DATA_ATTR int currentFrame = 0;
RTC_DATA_ATTR Frame frames[FRAMES_NUM];

int readCounterValue() {
  uint64_t io_data = REG_READ(GPIO_IN_REG);
  uint64_t io_data2 = REG_READ(GPIO_IN_REG);
  if (io_data != io_data2) {
    io_data = REG_READ(GPIO_IN_REG);
  }
  int value = 0;
  for (int i = 0; i < 8; i++) {
    if ((1ULL << pins[i]) & io_data) value |= 1 << i;
  }
  return value;
}

void setup() {
  for (int i = 0; i < 8; i++) pinMode(pins[i], INPUT);

  esp_sleep_wakeup_cause_t wakeup_reason = esp_sleep_get_wakeup_cause();  
  // print_wakeup_reason(wakeup_reason);

  esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);

  timeval current;
  switch (wakeup_reason) {
    default:
      // Reset everything
      gettimeofday(&current, nullptr);
      frames[0].seconds = current.tv_sec;
      frames[0].counter = readCounterValue();
      currentFrame = 1;
      esp_deep_sleep_start();
      break;
    case ESP_SLEEP_WAKEUP_EXT0:
    case ESP_SLEEP_WAKEUP_TIMER:
      // Proceed to measure.
      break;
  }

  gettimeofday(&current, nullptr);
  frames[currentFrame].counter = readCounterValue();
  frames[currentFrame].seconds = current.tv_sec;
  currentFrame++;

  esp_sleep_enable_ext0_wakeup(GPIO_NUM_14, digitalRead(14) ? 0 : 1);
  if (currentFrame < FRAMES_NUM) {
     esp_deep_sleep_start();
  }

  int bat = analogRead(A0);
  DynamicJsonDocument doc(1000);
  doc["bat_i"] = bat;
  doc["bat"] = bat * BAT_COEF;
  JsonArray vals = doc.createNestedArray("vals");

  int sum = 0;
  for (int i = 1; i < FRAMES_NUM; i++) {
    JsonObject item = vals.createNestedObject();
    int val = (frames[i].counter - frames[i - 1].counter) & 255;
    item["val"] = val;
    //item["c"] = frames[i].counter;
    sum += val;
    item["t"] = frames[i].seconds - frames[i - 1].seconds;
  }

  doc["sum"] = sum;
  time_t secondsPassed = frames[FRAMES_NUM - 1].seconds -  frames[0].seconds;
  doc["t"] = secondsPassed;

  float rpms = sum / (secondsPassed / 60.0f);
  doc["rpm"] = rpms;
  doc["speed"] = rpms * RPM_TO_SPEED;
  doc["speed2"] = rpms * RPM_TO_SPEED2;
  
  String s;
  serializeJson(doc, s);

  Serial.begin(115200);
  Serial.println(s);

  WRITE_PERI_REG(RTC_CNTL_BROWN_OUT_REG, 0);
  sendData(s);
  
  delay(100);
  Serial.flush();

  frames[0] = frames[FRAMES_NUM - 1];
  currentFrame = 1;
  esp_deep_sleep_start();
}

void loop() {
}
