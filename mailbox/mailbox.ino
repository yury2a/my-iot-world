// Mailbox-opened notifier.
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>
#include "my.h"

const long SEND_DATA_TIMEOUT = 30000;
const int ON_PIN = 4;

WiFiClient wifiClient;
PubSubClient mqttClient(wifiClient);

void sendData(String &data) {
  long end = millis() + SEND_DATA_TIMEOUT;

  WiFi.mode(WIFI_STA);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(WiFi.status());
    digitalWrite(BUILTIN_LED, HIGH); 
    delay(200);
    digitalWrite(BUILTIN_LED, LOW); 
    delay(300);
    if (end < millis()) {
      WiFi.disconnect();
      return;
    }
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  digitalWrite(BUILTIN_LED, HIGH);

// Loop until we're reconnected
  while (!mqttClient.connected()) {
    if (end < millis()) {
      WiFi.disconnect();
      digitalWrite(BUILTIN_LED, LOW); 
      return;
    }
    mqttClient.setServer(MQTT_SERVER, MQTT_SERVER_PORT);
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (mqttClient.connect("mailbox01", MQTT_USER, MQTT_PASSWORD)) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      String topic = "local/beacon/mailbox/";
      topic += WiFi.macAddress();
      mqttClient.publish(topic.c_str(), data.c_str());
    } else {
      Serial.print("failed, rc=");
      Serial.print(mqttClient.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(2000);
    }
  }

  mqttClient.disconnect();
  WiFi.disconnect();
  digitalWrite(BUILTIN_LED, LOW); 
}

void setup() {
  pinMode(ON_PIN, OUTPUT);
  digitalWrite(ON_PIN, HIGH);

  Serial.begin(115200);
  pinMode(BUILTIN_LED, OUTPUT);

  mqttClient.setBufferSize(2048);

  int bat = analogRead(A0);

  DynamicJsonDocument doc(1000);
  doc["action"] = "OPEN";
  doc["bat"] = bat / 73.0f;

  Serial.print("A0: ");
  Serial.println(bat);

  String s;
  serializeJson(doc, s);  
  sendData(s);

  Serial.println("Done.");

  digitalWrite(ON_PIN, LOW);
  ESP.deepSleep(0);
}

void loop() {

}
