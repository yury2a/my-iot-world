#include <stdint.h>
#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/pgmspace.h>

#define SAMPLE_RATE 16000
#define SPEAKER_PIN 3

const long DATA_SIZE = 1631488L;

#include <SPI.h>

// MX25L1606E
// https://www.tme.eu/Document/90cf95a7114025302d33a68125e207ab/MX25L1606E.pdf

// Sound
// https://playground.arduino.cc/Code/PCMAudio/

int writeData(int b = 0xff) {
  return SPI.transfer(b);
}

class OpGuard {
public:
  OpGuard() {
    SPI.beginTransaction(SPISettings(4000000, MSBFIRST, SPI_MODE0)); 
    digitalWrite(SS, LOW);
  }
  ~OpGuard() {
    digitalWrite(SS, HIGH);
    SPI.endTransaction();
  }
};

void rdid(int* mfr, int* memtype, int* memdens) {
  OpGuard op;
  writeData(0x9F);
  if (mfr) *mfr = writeData();
  if (memtype) *memtype = writeData();
  if (memdens) *memdens = writeData();
}

void read_data(long addr, uint8_t* buf, int len) {
  OpGuard op;
  writeData(0x03);
  writeData((addr >> 16) & 255);
  writeData((addr >> 8) & 255);
  writeData(addr & 255);
  for (int i = 0; i < len; i++)
    buf[i] = writeData();
}

const uint16_t BUF_LEN = 768;
unsigned char buf[BUF_LEN];
volatile uint16_t playPos = 0;
uint16_t bufPos;

ISR(TIMER1_COMPA_vect) {
#if SPEAKER_PIN == 11
    OCR2A = buf[playPos];
#else
    OCR2B = buf[playPos];
#endif

    if (++playPos >= BUF_LEN) {
      playPos = 0;
    }
}

void startPlayback()
{
    // Set up Timer 2 to do pulse width modulation on the speaker
    // pin.

    // Use internal clock (datasheet p.160)
    ASSR &= ~(_BV(EXCLK) | _BV(AS2));

    // Set fast PWM mode  (p.157)
    TCCR2A |= _BV(WGM21) | _BV(WGM20);
    TCCR2B &= ~_BV(WGM22);

#if SPEAKER_PIN == 11
        // Do non-inverting PWM on pin OC2A (p.155)
        // On the Arduino this is pin 11.
        TCCR2A = (TCCR2A | _BV(COM2A1)) & ~_BV(COM2A0);
        TCCR2A &= ~(_BV(COM2B1) | _BV(COM2B0));
        // No prescaler (p.158)
        TCCR2B = (TCCR2B & ~(_BV(CS12) | _BV(CS11))) | _BV(CS10);

        // Set initial pulse width to the first sample.
        OCR2A = buf[0];
#else
        // Do non-inverting PWM on pin OC2B (p.155)
        // On the Arduino this is pin 3.
        TCCR2A = (TCCR2A | _BV(COM2B1)) & ~_BV(COM2B0);
        TCCR2A &= ~(_BV(COM2A1) | _BV(COM2A0));
        // No prescaler (p.158)
        TCCR2B = (TCCR2B & ~(_BV(CS12) | _BV(CS11))) | _BV(CS10);

        // Set initial pulse width to the first sample.
        OCR2B = buf[0];
#endif

    // Set up Timer 1 to send a sample every interrupt.

    cli();

    // Set CTC mode (Clear Timer on Compare Match) (p.133)
    // Have to set OCR1A *after*, otherwise it gets reset to 0!
    TCCR1B = (TCCR1B & ~_BV(WGM13)) | _BV(WGM12);
    TCCR1A = TCCR1A & ~(_BV(WGM11) | _BV(WGM10));

    // No prescaler (p.134)
    TCCR1B = (TCCR1B & ~(_BV(CS12) | _BV(CS11))) | _BV(CS10);

    // Set the compare register (OCR1A).
    // OCR1A is a 16-bit register, so we have to do this with
    // interrupts disabled to be safe.
    OCR1A = F_CPU / SAMPLE_RATE;    // 16e6 / 8000 = 2000

    // Enable interrupt when TCNT1 == OCR1A (p.136)
    TIMSK1 |= _BV(OCIE1A);

    playPos = 0;
    sei();
}

const uint16_t CHUNK_SIZE = 64;

static_assert((CHUNK_SIZE & (CHUNK_SIZE - 1)) == 0);
static_assert(BUF_LEN % CHUNK_SIZE == 0);

long readPos;

void setup() {
  pinMode(SS, OUTPUT);
  pinMode(SPEAKER_PIN, OUTPUT);

  SPI.begin();

  delay(100);

  int mfr;
  rdid(&mfr, nullptr, nullptr);

  // Fill entire buffer
  read_data(0, buf, BUF_LEN);
  readPos = BUF_LEN;
  bufPos = 0;

  startPlayback();
}


void loop() {
  int played = (BUF_LEN + playPos - bufPos) % BUF_LEN;
  if (played >= CHUNK_SIZE) {
    read_data(readPos, buf + bufPos, CHUNK_SIZE);
    readPos += CHUNK_SIZE;
    if (readPos >= DATA_SIZE) {
      readPos = 0;
    }
    bufPos += CHUNK_SIZE;
    if (bufPos >= BUF_LEN) {
      bufPos = 0;
    }
  }
}
