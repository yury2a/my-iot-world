#include <Arduino.h>

#include <avr/interrupt.h>

// compareMatchReg = -[8, 000, 000Hz / (prescaler * desired interrupt frequency)]
const int compareMatchReg = -312;   // 100Hz = 8,000,000 / 256 / 100

volatile uint8_t interrupts_c = 0;

void setup() {
  DDRB = 0b0000; // All inputs
  PUEB = 0b0000; // No pullups
  PORTB = 0b0000; // digitalWrite(all, LOW)

  // initialize timer0
  cli(); // disable all interrupts
//  TCCR0A = 0;
  TCCR0B = (1 << CS02);    // 256 prescaler 
  TCNT0 = compareMatchReg; // preload timer
  TIMSK0 |= (1 << TOIE0);   // enable timer overflow interrupt
  sei(); // enable all interrupts
}


ISR(TIM0_OVF_vect)        // interrupt service routine 
{
  TCNT0 = compareMatchReg;   // preload timer
  interrupts_c++;
}

uint8_t display[] = {
  0b00010101, 0b01000101,
  0b01000110, 0b00100110,
  0b00100011, 0b00010011,
};

void noShow() {
  DDRB = 0;
}

void show(int i) {
  uint8_t b = display[i];
  uint8_t p = b >> 4;
  DDRB = b & 7;
  PORTB = p;
}

inline uint8_t now() { return interrupts_c; }
inline bool elapsed(uint8_t t, uint8_t n) { return uint8_t(interrupts_c - t) >= n; }
bool elapsed1sec(uint8_t t) { return elapsed(t, 100); }

void delay10m(uint8_t n) {
  uint8_t t = interrupts_c;
  while(uint8_t(interrupts_c - t) < n);
}

void somedelay() {
  for (volatile uint8_t t = 0xff; --t;);
}

void loop() {
  uint8_t t, t0;
  unsigned int i;
  // ping-pong
  i = 0;
  for (int seconds=5; --seconds;) {
	t = now();
	for (; !elapsed1sec(t); i++) {
      int f = i % 10;
      if (f >= 6) f = 10 - f;
      show(f);
  	  delay10m(20);
	}
  }

  // mid extension
  i = 0;
  for (int seconds=5; --seconds;) {
	t = now();
	for (; !elapsed1sec(t); i++) {
      int f = i % 6;
      if (f == 0) {
        noShow();
        delay10m(20);
        continue;
      }
      if (f >= 4) f = 6 - f;
      t0 = now();
      while (!elapsed(t0, 20)) {
        for (int j0 = 3 - f, j1 = 3 + f; j0 < j1; j0++) {        
          show(j0);
    	  somedelay();
        }
      }

	}
  }

  // long ping-pong
  i = 0;
  for (int seconds=5; --seconds;) {
	t = now();
	for (; !elapsed1sec(t); i++) {
      int f = i % 8;
      if (f >= 5) f = 8 - f;
      t0 = now();
      while (!elapsed(t0, 20)) {
        show(f);
    	somedelay();
        show(f + 1);
    	somedelay();
      }

	}
  }

  // solid
  t = now();
  for (i = 0; !elapsed1sec(t); i++) {
    show(i % 6);
	somedelay();
  }
}
