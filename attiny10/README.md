# Set of projects for ATTINY10

## Tornado

The reset pin fuse has to be pre-programmed after the flashing (and then high-voltage programmer has to be used)

```
avrdude -p t10 -c usbasp -U flash:w:tornado.hex
avrdude -p t10 -c usbasp -U fuse:w:0xfe:m
```