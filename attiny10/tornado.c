#include <Arduino.h>

#include <avr/interrupt.h>

// compareMatchReg = -[8, 000, 000Hz / (prescaler * desired interrupt frequency)]
const int compareMatchReg = -312;   // 100Hz = 8,000,000 / 256 / 100

volatile uint8_t interrupts_c = 0;

void setup() {
  DDRB = 0b0000; // All inputs
  PUEB = 0b0000; // No pullups
  PORTB = 0b0000; // digitalWrite(all, LOW)

  // initialize timer0
  cli(); // disable all interrupts
//  TCCR0A = 0;
  TCCR0B = (1 << CS02);    // 256 prescaler 
  TCNT0 = compareMatchReg; // preload timer
  TIMSK0 |= (1 << TOIE0);   // enable timer overflow interrupt
  sei(); // enable all interrupts
}


ISR(TIM0_OVF_vect)        // interrupt service routine 
{
  TCNT0 = compareMatchReg;   // preload timer
  interrupts_c++;
}

uint8_t display[] = {
  0b00010101, 0b00011001,
  0b00101010, 0b10001001,
  0b10001010, 0b01001100,

  0b10001100, 0b01000110,
  0b00100110, 0b00100011,
  0b00010011, 0b01000101,
};

void noShow() {
  DDRB = 0;
}

void show(int i) {
  uint8_t b = display[i];
  uint8_t p = b >> 4;
  DDRB = b & 15;
  PORTB = p;
}

inline uint8_t now() { return interrupts_c; }
inline bool elapsed(uint8_t t, uint8_t n) { return uint8_t(interrupts_c - t) >= n; }
bool elapsed1sec(uint8_t t) { return elapsed(t, 100); }
bool elapsedthirdsec(uint8_t t) { return elapsed(t, 33); }

void delay10m(uint8_t n) {
  uint8_t t = interrupts_c;
  while(uint8_t(interrupts_c - t) < n);
}

void somedelay() {
  for (volatile uint8_t t = 0xff; --t;);
}

void simple() {
  for (int i = 0; i < 24; i++) {
	uint8_t t = now();
    show(i % 12);
	while (!elapsedthirdsec(t));
  }
}

void vortex1() {
  for (int i = 0; i < 20; i++) {
    int l1 = i % 3, l2 = i % 4, l3 = i % 5;
	uint8_t t = now();
	while (!elapsedthirdsec(t)) {
      show(l1); somedelay();
      show(l2 + 3); somedelay();
      show(l3 + 7); somedelay();
	}
  }
}

void half() {
  for (int i = 0; i < 10; i++) {
	uint8_t t = now();
	while (!elapsedthirdsec(t)) {
	  for (int j = 0; j < 12; j += 2) {
        show(j); somedelay();
	  }
	}
	t = now();
	while (!elapsedthirdsec(t)) {
	  for (int j = 1; j < 12; j += 2) {
        show(j); somedelay();
	  }
	}
  }
}

void full() {
  uint8_t t = now();
  uint8_t i = 11;
  while (!elapsed1sec(t)) {
    show(i); if(!i--) i = 11;
	somedelay();
  }
}

void loop() {
  simple();
  vortex1();
  half();
  full();
  noShow();
  uint8_t t = now();
  while (!elapsed1sec(t));
}
