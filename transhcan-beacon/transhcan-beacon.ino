// Trashcan WiFi reporter.
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>
#include "my.h"

const int BUFFER_SIZE = 4000;
const int ACTIVE_PIN = 12;

void setup() {
  Serial.begin(115200);

  pinMode(ACTIVE_PIN, OUTPUT);
  digitalWrite(ACTIVE_PIN, LOW);
  pinMode(BUILTIN_LED, OUTPUT);

  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  WiFi.persistent(false);
  delay(100);

  Serial.println("Setup done");
  Serial.println(WiFi.macAddress());
}

WiFiClient wifiClient;
PubSubClient mqttClient(wifiClient);

const long SEND_DATA_TIMEOUT = 30000;
const long DEEP_SLEEP_TIMEOUT = 30L * 60000000L;

bool sendData(String &data) {
  long end = millis() + SEND_DATA_TIMEOUT;

  WiFi.mode(WIFI_STA);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(WiFi.status());
    digitalWrite(BUILTIN_LED, HIGH); 
    delay(200);
    digitalWrite(BUILTIN_LED, LOW); 
    delay(300);
    if (end < millis()) {
      WiFi.disconnect();
      return false;
    }
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  mqttClient.setBufferSize(BUFFER_SIZE);
  digitalWrite(BUILTIN_LED, HIGH); 

  // Loop until we're reconnected
  while (!mqttClient.connected()) {
    if (end < millis()) {
      WiFi.disconnect();
      digitalWrite(BUILTIN_LED, LOW); 
      return false;
    }
    mqttClient.setServer(MQTT_SERVER, MQTT_SERVER_PORT);
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (mqttClient.connect("arduinoClient", MQTT_USER, MQTT_PASSWORD)) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      String topic = "local/beacon/trashcan/";
      topic += WiFi.macAddress();
      mqttClient.publish(topic.c_str(), data.c_str());
    } else {
      Serial.print("failed, rc=");
      Serial.print(mqttClient.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(2000);
    }
  }

  mqttClient.disconnect();
  WiFi.disconnect();
  digitalWrite(BUILTIN_LED, LOW); 
  Serial.println("Done.");
  return true;
}

ADC_MODE(ADC_TOUT);

void loop() {
  Serial.println("scan start");

  int bat = analogRead(A0);

  DynamicJsonDocument doc(BUFFER_SIZE);

  Serial.print("A0: ");
  Serial.println(bat);
  
  doc["mac"] = WiFi.macAddress();
  doc["bat"] = bat;

  digitalWrite(BUILTIN_LED, HIGH); 

  int n = WiFi.scanNetworks();
  Serial.println("scan done");
  Serial.print(n);
  Serial.println(" networks found");
  for (int i = 0; i < n; ++i) {
    doc["nets"][i]["ssid"] = WiFi.SSID(i);
    doc["nets"][i]["rssi"] = WiFi.RSSI(i);

    Serial.print(WiFi.SSID(i));
    Serial.print(",");
    Serial.print(WiFi.RSSI(i));
    Serial.println("");
  }
  Serial.println("");
  digitalWrite(BUILTIN_LED, LOW); 
  delay(100);
  
  String s;
  serializeJson(doc, s);
  sendData(s);
  delay(100);

  // Sleep a bit before scanning again
  digitalWrite(ACTIVE_PIN, HIGH);
  ESP.deepSleep(DEEP_SLEEP_TIMEOUT);
}
