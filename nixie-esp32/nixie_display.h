const int DATA_IN = 26;
const int CLK = 33;
const int OE = 25;
const int HIGH_INV = LOW;
const int LOW_INV = HIGH;

#define SmallDelay(n) { volatile int i; for(i=0; i<n; ++i) {} }

void pushBit(int b) {
  digitalWrite(CLK, HIGH_INV);
  SmallDelay(60);
  digitalWrite(DATA_IN, b ? HIGH_INV : LOW_INV);
  SmallDelay(50);
  digitalWrite(CLK, LOW_INV);
  SmallDelay(100);
} 

int pinSet;

void initNixie() {
  pinMode(OE, OUTPUT);
  digitalWrite(OE, LOW_INV);
  pinMode(DATA_IN, OUTPUT);
  pinMode(CLK, OUTPUT);
  digitalWrite(CLK, LOW_INV);

  for (int i = 0; i < 32; i++) {
    pushBit(0);
  }
  pinSet = 33;
  digitalWrite(OE, HIGH_INV);  
}

void setPin(int n) {
  if (n >= pinSet) {
    for (int i = pinSet; i < n; i++) {
      pushBit(0);
    }
    pinSet = n;
    return;
  }
  while (pinSet + n <= 32) {
    pushBit(0);
    pinSet++;    
  }
  pushBit(1);
  for (int i = 1; i < n; i++) {
    pushBit(0);
  }
  pinSet = n;  
}

int pins[] = {
  12, 11, 15, 9, 8, 7, 16, 10, 14, 13,
  23, 21, 26, 19, 18, 17, 27, 20, 25, 24
};

void displayDigits(int d1, int d2) {
  if (d1 < 0 && d2 < 0) {
    setPin(33);
    return;
  }
  for (int i = 0; i < 50; i++) {
    if (d1 >= 0) {
      digitalWrite(OE, LOW_INV);
      setPin(pins[d1]);
      digitalWrite(OE, HIGH_INV);
      delay(10);
    }

    if (d2 >= 0) {
      digitalWrite(OE, LOW_INV);
      setPin(pins[d2 + 10]);
      digitalWrite(OE, HIGH_INV);
      delay(10);
    }
  }
}

void showNumbers() {
  static int j = 0;
  displayDigits((j / 10) % 10, (j % 10) + 10);
  j++; if (j >= 100) j = 0;  
}
