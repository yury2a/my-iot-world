
void civil_from_days(unsigned long z, int *py, int *pm, int *pd, int *pw)
{
  struct tm * ti;
  time_t rawtime = z;
  ti = localtime (&rawtime);
  *py = ti->tm_year + 1900;
  *pm = (ti->tm_mon + 1) < 10 ? 0 + (ti->tm_mon + 1) : (ti->tm_mon + 1);
  *pd = (ti->tm_mday) < 10 ? 0 + (ti->tm_mday) : (ti->tm_mday);
  *pw = ti->tm_wday;
}

unsigned long days_from_civil(unsigned y, unsigned m, unsigned d)  {
    struct tm ti;
    memset(&ti, 0, sizeof ti);
    ti.tm_year = y - 1900;
    ti.tm_mon = m - 1;
    ti.tm_mday = d;
    return mktime(&ti);
}

const unsigned long CST_OFFSET = -6 * 3600L;
const unsigned long CDT_DELTA = 3600L;

bool is_cdt(int m, int d, int wd, int h) {  
  if (3 < m && m < 11) return true;
  if (m < 3 || (m == 3 && d < 8) ||
      m > 11 || (m == 11 && d >= 8)) return false;

  if (m == 3) {
    int s = ((6 + d - wd) % 7) + 9;
    return s < d || (d == s && h < 2);
  } else {
    int s = ((6 + d - wd) % 7) + 1;
    return s > d && !(d == s && h < 2);
  }
}

bool get_time(long unsigned z, int *h, int *m, int *s, int *dm, int *dd, int *dy, int* dwd, int *ofs) {
  struct tm * ti;
  time_t rawtime = z;
  ti = localtime (&rawtime);
  z += CST_OFFSET;
  *ofs = CST_OFFSET;
  
  int y, mo, d, wd;
  civil_from_days(z, &y, &mo, &d, &wd);
  if(y < 2007) return false;

  if (is_cdt(mo, d, wd, (z % 86400L) / 3600)) {
    z += CDT_DELTA;
    *ofs += CDT_DELTA;
    // TODO fix date if z changed
  }
  
  *h = (z % 86400L) / 3600;
  *m = (z % 3600) / 60;
  *s = z % 60;
  *dm = mo;
  *dd = d;
  *dy = y;
  *dwd = wd;
  return true;
}

WiFiUDP ntpClient;

uint32_t get_ntp_time(const char *host) {
  const int NTP_PACKET_SIZE = 48;
  byte ntpPacketBuffer[NTP_PACKET_SIZE];

  IPAddress address;
  WiFi.hostByName(host, address);
  memset(ntpPacketBuffer, 0, NTP_PACKET_SIZE);
  ntpPacketBuffer[0] = 0b11100011;   // LI, Version, Mode
  ntpPacketBuffer[1] = 0;     // Stratum, or type of clock
  ntpPacketBuffer[2] = 6;     // Polling Interval
  ntpPacketBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  ntpPacketBuffer[12]  = 49;
  ntpPacketBuffer[13]  = 0x4E;
  ntpPacketBuffer[14]  = 49;
  ntpPacketBuffer[15]  = 52;
  ntpClient.beginPacket(address, 123); //NTP requests are to port 123
  ntpClient.write(ntpPacketBuffer, NTP_PACKET_SIZE);
  ntpClient.endPacket();

  delay(1000);
  
  int packetLength = ntpClient.parsePacket();
  if (packetLength){
    if(packetLength >= NTP_PACKET_SIZE){
      ntpClient.read(ntpPacketBuffer, NTP_PACKET_SIZE);
    }
    ntpClient.flush();
    uint32_t secsSince1900 = (uint32_t)ntpPacketBuffer[40] << 24 | (uint32_t)ntpPacketBuffer[41] << 16 | (uint32_t)ntpPacketBuffer[42] << 8 | ntpPacketBuffer[43];
    uint32_t epoch = secsSince1900 - 2208988800UL;
    return epoch;
  }
  return 0;
}

uint32_t days_till(uint32_t epoch, unsigned fm, unsigned fd) {
  int h, m, s, dm, dd, dy, wd, ofs;
  get_time(epoch, &h, &m, &s, &dm, &dd, &dy, &wd, &ofs);
  int fy = dy;
  if (fm < dm || (fm == dm && fd < dd)) fy++;
  const uint32_t SECONDS_IN_A_DAY = 24 * 3600;
  uint32_t diff = days_from_civil(fy, fm, fd) / SECONDS_IN_A_DAY - (epoch + ofs) / SECONDS_IN_A_DAY;
  return diff;
}
