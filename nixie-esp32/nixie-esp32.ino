#include <WiFiManager.h> // https://github.com/tzapu/WiFiManager
#include <EEPROM.h>

#include "nixie_display.h"
#include "time_utils.h"

#define TRIGGER_PIN 0

const int INVALID_DAYS = 400;

String dueDate;
int daysLeft = INVALID_DAYS;
unsigned long daysUpdated;

void updateDays() {
  int delim = dueDate.indexOf('/') < 0 ? dueDate.indexOf('-') : dueDate.indexOf('/');
  if (delim <= 0 || delim + 1 >= dueDate.length()) {
    daysLeft = INVALID_DAYS;
    return;
  }
  int month = atoi(dueDate.substring(0, delim).c_str());
  int day = atoi(dueDate.substring(delim + 1).c_str());
  uint32_t epoch;
  if ((epoch = get_ntp_time("time.nist.gov"))) {
    daysLeft = days_till(epoch, month, day);
  } else {
    daysLeft = INVALID_DAYS;
  }
  Serial.print("Days left: ");
  Serial.println(daysLeft);
  daysUpdated = millis();
}

bool shouldSaveConfig = false;

//callback notifying us of the need to save config
void saveConfigCallback () {
  Serial.println("Should save config");
  shouldSaveConfig = true;
}


const int EEPROMDataMagic = 124;
struct EEPROMData {
  int magic;
  char dueDate[6];
};

void loadDueDate() {
  EEPROMData data;
  EEPROM.begin(512);
  EEPROM.get(0, data);
  EEPROM.end();
  if (data.magic == EEPROMDataMagic) {
    dueDate = String(data.dueDate);
    Serial.print("Config loaded: ");
    Serial.println(dueDate);
  } else {
    Serial.println("no config");
  }
}

void saveDueDate() {
  EEPROMData data;
  data.magic = EEPROMDataMagic;
  memcpy(data.dueDate, dueDate.c_str(), dueDate.length() + 1);
  Serial.print("Saving config: ");
  Serial.println(dueDate);
  
  EEPROM.begin(512);
  EEPROM.put(0, data);
  EEPROM.commit();
  EEPROM.end();
}

void setup() {
  initNixie();

  // WiFi.mode(WIFI_STA); // explicitly set mode, esp defaults to STA+AP

  Serial.begin(115200);
  
  loadDueDate();

  WiFiManager wm;

  // reset settings - wipe stored credentials for testing
  // these are stored by the esp library
  // wm.resetSettings();

  wm.setSaveConfigCallback(saveConfigCallback);

  WiFiManagerParameter s("expire", "Due date", dueDate.c_str(), 6);
  wm.addParameter(&s);

  bool res;
  res = wm.autoConnect(); // auto generated AP name from chipid

  if(!res) {
      Serial.println("Failed to connect");
      ESP.restart();
  } 
  else {
      //if you get here you have connected to the WiFi    
      Serial.println("connected...yeey :)");
  }

  pinMode(TRIGGER_PIN, INPUT_PULLUP);
  dueDate = String(s.getValue());
  if (shouldSaveConfig) {
    saveDueDate();
  }
  updateDays();
}

void checkForConfig() {
  if (digitalRead(TRIGGER_PIN)) return;

  int timeout = 120; // seconds to run for

  WiFiManager wm;

  //reset settings - for testing
  //wm.resetSettings();

   WiFiManagerParameter s("expire", "Due date", dueDate.c_str(), 6);
   wm.addParameter(&s);

   shouldSaveConfig = false;
   wm.setSaveConfigCallback(saveConfigCallback);

   // set configportal timeout
   wm.setConfigPortalTimeout(timeout);

  if (!wm.startConfigPortal()) {
    Serial.println("failed to connect and hit timeout");
    delay(3000);
    //reset and try again, or maybe put it to deep sleep
    ESP.restart();
    delay(5000);
  }

  dueDate = String(s.getValue());
  if (shouldSaveConfig) {
    saveDueDate();
  }

  //if you get here you have connected to the WiFi
  Serial.println("connected...yeey :)");
  updateDays();
}


void loop() {
  checkForConfig();
  if (millis() - daysUpdated > 3600000UL) updateDays();
  //showNumbers();
  if (daysLeft == INVALID_DAYS) {
    displayDigits(0, -1);
  } else if (daysLeft < 100) {
    int firstDigit = (daysLeft / 10) % 10;
    int lastDigit = daysLeft % 10;
    displayDigits(firstDigit > 0 ? firstDigit : -1, lastDigit);
  } else {
    displayDigits(9, -1);
  }
}
