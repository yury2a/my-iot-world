#include <avr/power.h>
#include <avr/sleep.h>

int senseEnablePin = 2;
int openPin = 3;

long start;

void setup() {
  pinMode(senseEnablePin, OUTPUT);
  pinMode(openPin, INPUT);
  pinMode(LED_BUILTIN, OUTPUT);

  digitalWrite(senseEnablePin, HIGH);

  start = millis();
}

void stopHardware() {
  // Disable everything
  pinMode(senseEnablePin, INPUT);
  pinMode(openPin, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);
  
  ADCSRA = 0;
  noInterrupts();
  CLKPR = 0x80;
  CLKPR = 7;
  power_all_disable();

  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  sleep_bod_disable();
  sleep_enable();
  sleep_cpu();
  while(1); // ... and stop
}

const int TRIES = 10;

void loop() {
  digitalWrite(senseEnablePin, LOW);
  delay(1);
  // Check distance TRIES times.
  int c = 0;
  for (int i = 0; i < TRIES; i++) { 
    if (digitalRead(openPin)) c++;
    delay(1);
  }
  digitalWrite(senseEnablePin, HIGH);
  delay(100);

  bool blocked = c > TRIES / 2;
  digitalWrite(LED_BUILTIN, blocked ? HIGH : LOW);

  if ((millis() - start) > 600000L) { // 10 min
    stopHardware();
  }
}
