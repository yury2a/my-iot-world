# Car Bounds Indicator

Ultrasonic sensor to indicate where (how far) a car in
relation to garage door. LEDs are lit if a car is near.
