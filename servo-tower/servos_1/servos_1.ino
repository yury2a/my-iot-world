/*
 Controlling servos using serial interface.
*/

#include <Servo.h>

Servo s1;
Servo s2;

void setup() {
  s1.attach(5);
  s2.attach(6);
  Serial.begin(9600);
  
  s1.write(90);
  s2.write(90);

  while(!Serial);
  Serial.write("TOWER READY\n");
}

int read() {
  while (!Serial.available());
  return Serial.read();  
}

int read_number(int* last) {
  char buf[12];
  int i = 0;
  do {
    int ch = read();
    if (ch < '0' || ch > '9') {
        *last = ch;
        break;
    }
    buf[i++] = ch;
  } while (i < 10);
  buf[i] = 0;
  return atoi(buf);
}

void ignore_line(int ch) {
  Serial.write("ERR !> ");
  Serial.write(ch);
  if (ch == '\n') return;
  do {
    ch = read();
    Serial.write(ch);
  } while (ch != '\n');
}

void show_current() {
  int ch = read();
  if (ch != '\n') { ignore_line(ch); return; }

  Serial.write("P");
  char buf[12];
  Serial.write(itoa(s1.read(), buf, 10));
  Serial.write(',');
  Serial.write(itoa(s2.read(), buf, 10));
  Serial.write('\n');
}

void read_move() {
  int ch;
  int r = read_number(&ch);
  if (ch != ',') { ignore_line(ch); return; }
  int t = read_number(&ch);
  if (ch != '\n') { ignore_line(ch); return; }
  if (t < 0 || t > 180 || r < 0 || r > 180) {
    Serial.write("ERR OUT\n");
    return;
  }
  s1.write(t);
  s2.write(r);
  Serial.write("OK\n");
}

void show_help() {
  int ch = read();
  if (ch != '\n') { ignore_line(ch); return; }
  Serial.write("Mr,t - sets servos: r=rotatio (0-180), t=tilt (0-180)\n");
  Serial.write("R    - reads current position\n");
  Serial.write("H    - this help\n");
}

void loop() {
  if (Serial.available()) {
    int ch = Serial.read();
    switch (ch) {
      case 'M': read_move(); break;
      case 'R': show_current(); break;
      case 'H': show_help(); break;
      default:
        Serial.write("Unknown command (use 'H')\n");
        ignore_line(ch); break;
    }
  }
}
