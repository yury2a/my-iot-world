// i2c controlling the tower servos
#include <Servo.h>
#include <Wire.h>

Servo s1;
Servo s2;

void setup() {
  s1.attach(5);
  s2.attach(6);
  
  s1.write(90);
  s2.write(90);

  Wire.begin(24);
  Wire.onReceive(receiveEvent);
}

int r1 = -1;
int r2 = -1;
void loop() {
  if (r1 >= 0) { s1.write(r1); r1 = -1; }
  if (r2 >= 0) { s2.write(r2); r2 = -1; }
}

int reg;

void receiveEvent(int howMany)
{
  reg = Wire.read();
  if (howMany > 1) {
    int val = Wire.read();
    switch (reg) {
      case 0:
        r1 = val;
        break;
      case 1:
        r2 = val;
        break;
    }
  }
}
