#include <Stepper.h>
#include <Servo.h>

#define LOG_SENSE
#define BLINK_SENSE

namespace Clock {
  
const int STEPS_PER_REVOLUTION = 2038;

const int SENSE_PIN = 4;
const int SERVO_PIN = 6;
const int CLOCK_DISABLED_PIN = 3;
const int ENABLE_PIN = A1;

const unsigned long SERVO_PULSE = 300;
const int STEPPER_SPEED = 15;

const int ENGAGE_STEPPER_ANGLE = 90;
const int DISENGAGE_STEPPER_ANGLE = 0;

const int KeyHours = 2;
const int KeyMinutes = 8;
const int KeySeconds = 9;

Stepper stepper(STEPS_PER_REVOLUTION, 8, 10, 9, 11);
Servo servo;

void enable() {
  digitalWrite(ENABLE_PIN, HIGH);
}

void disable() {
  digitalWrite(ENABLE_PIN, LOW);
}

void disableStepper() {
  digitalWrite(8, LOW); digitalWrite(9, LOW);
  digitalWrite(10, LOW); digitalWrite(11, LOW);
}

void setServo(int d) {
  servo.attach(SERVO_PIN);
  servo.write(d);
  delay(SERVO_PULSE);
  servo.detach();  
}

void findSeconds();

void stopWithError();

class FindKey {
  static const int HS = 7;
  int history[HS];
  int hr = 0, hw = 0;
  int state = 0;
  int onC1, offC1, onC2;

  void addHistory(int i) {
    history[hw] = i;
    hw = (hw + 1) % HS;
    if (hw == hr) hr = (hr + 1) % HS;
  }
 public:
  void reset() {
    hr = 0; hw = 0;
    state = 0;
  }
  void on(int c, int d) {
    if (state == 0) {
      onC1 = c;
    } else {
      onC2 = c;
    }
  }
  void off(int c, int d) {
    if (c < 0 || c > 500) {
      state = 0;
    }
    if (c > 88 && state == 0) {
      // hour arrow
      state = 1;
      offC1 = c;
      return;
    }
    if (state == 1) {
#ifdef LOG_SENSE      
    Serial.println("-- key found");
#endif
      findSeconds();
      addHistory(onC1 + offC1 + onC2 + c);
      return;
    }
    addHistory(onC1 + c);
#ifdef LOG_SENSE      
    Serial.println("-- minute");
#endif
  }
  int calcMid() {
    if (hr > 0) {
      int i = hw < hr ? hw : 0, j = hw < hr ? HS : hw;
      while (i < j) {
        history[i++] = history[hr++]; 
      }
      hr = 0; hw = i;
    }
    for (int i = 0; i < hw - 1; i++) {
      for (int j = i + 1; j < hw; j++) {
        if (history[i] > history[j]) {
          int t = history[j]; history[j] = history[i]; history[i] = t;
        }
      }
    }
    return history[hw / 2];
  }
} fk;

class TrackCount {
  const int NOISE_LIMIT = 5;
  int last = HIGH;
  int filterCount = 0;
  int count = 0;
  void report(int i, int c, int d) {
#ifdef LOG_SENSE          
    Serial.print(i); Serial.print(" ");
    Serial.print(c); Serial.print(" ");
    Serial.print(d); Serial.println();
#endif
    if (i) fk.on(c, d); else fk.off(c, d);
  }
 public:
   void reset() {
     last = HIGH;
     filterCount = 0;
     count = 0;
     fk.reset();
   }
   void check(int i) {
     if (i == last) {
       if (filterCount > 0) {
        count += filterCount; filterCount = 0;
       }
       count++;
       if (count > 5000) {
#ifdef LOG_SENSE
        Serial.println("error: no sense");    
#endif
        stopWithError();
        return;
       }
     } else {
       filterCount++;
       if (filterCount > NOISE_LIMIT) {
         report(last, count, filterCount);
         last = i;
         count = filterCount; filterCount = 0;
       }
     }
   }
} tc;

enum FindState {
  IDLE,
  FIND_KEY,
  FIND_SECONDS,
  KEY_FOUND,
  ERROR,
} fs;

int currentHours, currentMinutes;
int currentMinutesFrac /* in sec */, currentSeconds;
int midStepsPerHour;

void findKey() {
  fs = FindState::FIND_KEY;
  setServo(ENGAGE_STEPPER_ANGLE);
  stepper.step(10);
  digitalWrite(CLOCK_DISABLED_PIN, HIGH);
  currentHours = KeyHours;
  currentMinutes = KeyMinutes;
}

void keyFound() {
  fs = FindState::KEY_FOUND;
  digitalWrite(CLOCK_DISABLED_PIN, HIGH);

  // Default steps per hour
  int c = fk.calcMid();
  const int DefaultTicksPerHour = 2981;
  if (abs(c - DefaultTicksPerHour) > 100) {
    c = DefaultTicksPerHour;
  }
#ifdef LOG_SENSE
  Serial.print("-- mid: "); Serial.println(c);    
#endif
  midStepsPerHour = c;

  //setServo(DISENGAGE_STEPPER_ANGLE);
  //disableStepper();
}

class FilteredPin {
  static const int NOISE_FILTER = 5;
  int pin;
  int invCount;
  int current;
 public:
   FilteredPin(int pin_) : pin(pin_) {
    current = digitalRead(pin);
    invCount = 0;
   }
   int read() {
    int i = digitalRead(pin);
    if (i != current) {
      if (++invCount > NOISE_FILTER) {
        current = i;
        invCount = 0;
      }
    } else {
      invCount = 0;
    }
    return current;
   }
};

void findSeconds() {
  unsigned long t;
  fs = FindState::FIND_SECONDS;
  digitalWrite(CLOCK_DISABLED_PIN, LOW);
  t = millis();
  delay(1000);
  FilteredPin s(SENSE_PIN);
  while (s.read()) {
    if (millis() - t > 120000UL) {
#ifdef LOG_SENSE
      Serial.println("error: no seconds");    
#endif
      stopWithError();
      return;
    }
  }
#ifdef LOG_SENSE
   Serial.println("-- seconds found");    
#endif
  while (!s.read()) {
    if (millis() - t > 120000UL) {
#ifdef LOG_SENSE
      Serial.println("error: no seconds");    
#endif
      stopWithError();
      return;
    }
  }
  unsigned long t0 = millis() - t;
  currentMinutesFrac = t0 / 1000UL;
  currentSeconds = KeySeconds;
  delay(1000);
  keyFound();
}

void setIdle() {
  fs = FindState::IDLE;
  tc.reset();

  enable();
  setServo(DISENGAGE_STEPPER_ANGLE);
  disableStepper();
  disable();

  digitalWrite(CLOCK_DISABLED_PIN, LOW);
}

void stopWithError() {
  fs = FindState::ERROR;
  digitalWrite(CLOCK_DISABLED_PIN, LOW);
  disable();
}

void setTime(int h, int m) {
  const int c = midStepsPerHour;
  long expected = h * 3600L + m * 60L + currentSeconds;
  long actual = currentHours * 3600L + currentMinutes * 60L + currentMinutesFrac;
  long diff = ((expected - actual) + 86400L) % 43200L;

  int dh = diff / 3600L;
  int dm_s = diff % 3600L;

  if (dh > 0) {
    // loop below will reset currentMinutesFrac difference, compensate
    dm_s += currentMinutesFrac;
  }
  
#ifdef LOG_SENSE
    Serial.print("-- rotate hour: ");
    Serial.println(dh);
#endif
  while (dh > 0) {
    const int AutoAdjust = 200;
    stepper.step(c - AutoAdjust);
    int c2 = 2 * AutoAdjust;
#ifdef BLINK_SENSE
    digitalWrite(LED_BUILTIN, LOW);
#endif
    FilteredPin s(SENSE_PIN);
    while (c2 > 0 && s.read() == HIGH) {
      stepper.step(1);
      c2--;
    }
    while (c2 > 0 && s.read() == LOW) {
      stepper.step(1);
      c2--;
    }
#ifdef BLINK_SENSE
    digitalWrite(LED_BUILTIN, HIGH);
#endif
    dh--;
  }
#ifdef LOG_SENSE
  Serial.print("-- rotate minutes (in sec): ");
  Serial.println(dm_s);
#endif
  stepper.step(long(dm_s) * c / 3600UL);
}

void setTime(int h, int m, int s) {
  if (s < currentSeconds) {
    s += 60;
    m--;
  }
  setTime(h, m);
  digitalWrite(CLOCK_DISABLED_PIN, LOW);
  delay((s - currentSeconds) * 1000L);
  digitalWrite(CLOCK_DISABLED_PIN, HIGH);
}

void syncTime(int h, int m, unsigned long at) {
  setTime(h, m);
#ifdef BLINK_SENSE
      digitalWrite(LED_BUILTIN, LOW);
#endif
  long start = at + currentSeconds * 1000L;
  long p = start - millis();
  long mm = 0;
  while (p < 0) {
    mm++;
    p += 60000L; 
  }
  delay(p);
  digitalWrite(CLOCK_DISABLED_PIN, LOW);
#ifdef LOG_SENSE
    Serial.print("-- delayed by (sec): ");
    Serial.print(p);
    Serial.print("; catching up minutes: ");
    Serial.println(mm);
#endif
  stepper.step(mm * midStepsPerHour / 60);
  
}

void setup() {
  stepper.setSpeed(STEPPER_SPEED);

  pinMode(SENSE_PIN, INPUT);
  pinMode(CLOCK_DISABLED_PIN, OUTPUT);
  pinMode(ENABLE_PIN, OUTPUT);

  disable();
  fs = FindState::IDLE;
}

void loop() {
  switch (fs) {
    case FindState::FIND_KEY: {
      stepper.step(1);
      int i = digitalRead(SENSE_PIN);
      tc.check(i);
#ifdef BLINK_SENSE
      digitalWrite(LED_BUILTIN, i);
#endif
      break;
    }
  }
}

}
