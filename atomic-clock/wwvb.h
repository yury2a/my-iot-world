namespace WWVB {

#define LOG_WWVB
#define BLINK_WWVB

  const int SIGNAL_PIN = 2;
  
  bool searchBeat;
  bool searchFrame;
  bool receving;
  bool incompleteFrame;

  struct Result {
    int timeFound;
    int h;
    int m;
    int dst;
    unsigned long at;
  } result;
  
  void search() {
    result.timeFound = 0;
    searchBeat = true;    
  }

  void setup() {
    pinMode(SIGNAL_PIN, INPUT);
    search();
  }
  
  template<typename T>
  inline bool is_about(T i, const T x, const T e) {
    return (x - e) <= i && i <= (x + e);
  }

  inline void led(int i) {
#ifdef BLINK_WWVB  
    digitalWrite(LED_BUILTIN, i);
#endif    
  }
  
  void beat() {
    int s;
    unsigned long t, t1, t2;
    s = digitalRead(SIGNAL_PIN);
    led(s);
    while (!s) { s = digitalRead(SIGNAL_PIN); }
    led(s);
    while (true) {
      do {
        t1 = millis();
        while (s) { s = digitalRead(SIGNAL_PIN); }
        led(s);
        t2 = millis();
        while (!s) { s = digitalRead(SIGNAL_PIN); }
        led(s);
        t = millis() - t1;
      } while (!is_about(t, 1000UL, 100UL));
      int c = 0;
      while (c < 3) {
        led( c & 1);
        t1 = millis();
        while (s) { s = digitalRead(SIGNAL_PIN); }
        while (!s) { s = digitalRead(SIGNAL_PIN); }
        t = millis() - t1;
        c++;
      }
      if (!is_about(t, 1000UL, 100UL)) continue;
      break;
    }  
  }
  
  bool frame() {
    unsigned long t1 = millis();
    bool foundMarker = false;
    int gap = 0;
    while (true) {
      unsigned long t0 = t1;
      int c1 = 0, c0 = 0;
      while (millis() - t0 < 850) {
        if (digitalRead(SIGNAL_PIN)) c1++; else c0++;
        delay(1);
      }
      while (!digitalRead(SIGNAL_PIN)) {
        c0++;
        delay(1);
      }
      t1 = millis();
      unsigned long t = t1 - t0;
      if (!is_about(t, 1000UL, 100UL) || c1 < 100) return false;
      bool marker = c1 > 700;
      led(marker);
      if (marker && foundMarker) {
        break;
      }
      if (marker) {
        gap = 0;
      } else {
        gap++;
        if (gap > 9) return false;
      }
      foundMarker = marker;
    }
    //delay(58900);
    incompleteFrame = true;
    return true;
  }
  
  bool getBits(int8_t* data, int i, int j, int *res) {
    int n = 0;
    while (i < j) {
      n <<= 1;
      switch (data[i]) {
        case 0: break;
        case 1: n++; break;
        default: return false;
      }
      i++;
    }
    *res = n;
    return true;
  }
  
  int readErrors = 0;
  
  bool skipLow() {
    unsigned long t = millis();
    int c = 0;
    while (c < 3) {
      if (digitalRead(SIGNAL_PIN)) { c++; } else { c = 0; }
      if (millis() - t > 250) return false;
      delay(1);
    }
    return true; 
  }
  
  bool readFrame() {
    int8_t data[59];
    int i = 0;
    // Frame starts
#ifdef LOG_WWVB    
    Serial.print(">");
#endif    
    if (incompleteFrame) {
#ifdef LOG_WWVB    
      Serial.print("2");
#endif    
      data[i++] = 2;
      incompleteFrame = false;
    } else {
      if (!skipLow()) return false;
    }
    for (; i < 59; i++) {
      int c = 0;
      unsigned long t = millis();
      while (millis() - t < 850) {
        if (digitalRead(SIGNAL_PIN)) c++;
        delay(1);
      }
      if (!skipLow() || !is_about(millis() - t, 1000UL, 100UL)) return false;
  
      int d;
      if (c < 350) {
        if (c >= 150) {
          d = 0;
        } else {
          d = 3;
        }
      } else {
        if (c < 700) {
          d = 1;
        } else {
          d = 2;
        }
      }
      data[i] = d;
      led(i & 1);
#ifdef LOG_WWVB    
      Serial.print(d);
#endif
    }
#ifdef LOG_WWVB        
    Serial.print(" .. ");
#endif
    int m1, m2, h1, h2, d1, d2, d3, y1, y2, dst;
    bool ok_m = getBits(data, 1, 4, &m1) & getBits(data, 5, 9, &m2);
    bool ok_h = getBits(data, 12, 14, &h1) & getBits(data, 15, 19, &h2);
    bool ok_d = getBits(data, 22, 24, &d1) & getBits(data, 25, 29, &d2) & getBits(data, 30, 34, &d3);
    bool ok_y = getBits(data, 45, 49, &y1) & getBits(data, 50, 54, &y2);
    bool ok_dst = getBits(data, 57, 59, &dst);
    bool ok_markers = data[0] == 2 && data[9] == 2 && data[19] == 2 &&
                       data[29] == 2 && data[39] == 2 && data[49] == 2;
    if (ok_m && ok_h && ok_d && ok_y && ok_dst && ok_markers) {
#ifdef LOG_WWVB          
      Serial.print(h1 * 10 + h2); Serial.print(":");
      Serial.print(m1 * 10 + m2); Serial.print(" ");
      Serial.print(d1 * 100 + d2 * 10 + d3); Serial.print("'");
      Serial.print(y1 * 10 + y2); Serial.print("+");
      Serial.print(dst); Serial.println();
#endif
      result.timeFound++;
      result.h = h1 * 10 + h2;
      result.m = m1 * 10 + m2;
      result.dst = dst;
      result.at = millis() - 59000UL;
      
      readErrors = 0;
    } else {
#ifdef LOG_WWVB          
      Serial.println("ERR");
#endif
      if (readErrors > 1) {
        readErrors = 0;
        return false;
      }
      readErrors++;    
    }
    delay(850);
    return true;
  }

  void loop() {
    if (searchBeat) {
#ifdef LOG_WWVB          
      Serial.println("Searching for signal...");
#endif
      beat();
      searchBeat = false;
      searchFrame = true;
      receving = false;
      result.timeFound = 0;
      led(LOW);
#ifdef LOG_WWVB    
      Serial.println("Found. Looking for markers...");
#endif
    }
    if (searchFrame) {
      if (!frame()) {
        searchBeat = true; return;
      }
      searchFrame = false;
      receving = true;
      led(LOW);
#ifdef LOG_WWVB
      Serial.println("Sync'ed");
#endif
    }
    if (!readFrame()) {
      searchBeat = true;
    }
  }
  
}
