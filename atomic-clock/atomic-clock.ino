#include "clock_find.h"
#include "wwvb.h"

int state = 0;

const int BUTTON_PIN = A0;

const int ZONE_DST_OFFSET = -5;
const int ZONE_OFFSET = -6;

void setup() {
  Serial.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(BUTTON_PIN, INPUT_PULLUP);

  WWVB::setup();
  Clock::setup();
  Clock::setIdle();
  delay(2000);
}

const int ConsistentTimeAmount = 1;

unsigned long t;

void loop() {
  const auto & result = WWVB::result;
  switch (state) {
    case 0: // initial state
    case 4: // reacquire time
      if (result.timeFound >= ConsistentTimeAmount) {
        if (state == 4) {
          // back to idle
          state = 2;
          break;
        }
        state = 1;
        Clock::enable();
        Clock::findKey();
        return;
      }
      WWVB::loop();
      break;
    case 1: // rewind clock
      if (Clock::fs == Clock::KEY_FOUND) {
        int dst = (result.dst & 1);
        int h = result.h + (dst ? ZONE_DST_OFFSET : ZONE_OFFSET);
        int m = result.m;
        const int ExtraMinutes = 1;
        unsigned long p = (millis() - result.at) / 60000UL;
        int delta = p + ExtraMinutes;
        m += delta;
        while (m > 59) { h++; m -= 60; }
        h = (h + 12) % 12;
        Serial.print("Set time to: ");
        Serial.print(h); Serial.print(":"); Serial.println(m);
        Clock::syncTime(h, m, result.at + delta * 60000UL);
        Clock::setIdle();
        Clock::disable();
        t = millis();
        state = 2;
        break;
      } else if (Clock::fs == Clock::ERROR) {
        Clock::setIdle();
        Clock::disable();
        t = millis();
        state = 3;
        digitalWrite(LED_BUILTIN, LOW);      
        break;   
      }
      Clock::loop();
      break;
    case 2: // idle
      if (digitalRead(BUTTON_PIN) == LOW) {
        state = 1;
        Clock::enable();
        Clock::findKey();
        break;
      }
      // reaquire time after 1.5 hours
      if ((millis() - t) > 5400000ULL) {
        state = 4;
        WWVB::search();
        break;
      }
      {
        unsigned anim_t = (millis() - t) % 10000;
        int d = 0; // duty cycle 0..15
        if (anim_t <= 1000) {
          d = 16 - abs(500 - int(anim_t)) / 32;
        }
        digitalWrite(LED_BUILTIN, (anim_t % 32) < d);
      }
      break;
    case 3: // error state
      // reset to initial state if button pressed or after an hour
      if (digitalRead(BUTTON_PIN) == LOW ||
          (millis() - t) > 3600000ULL) {
        state = 0;
        WWVB::search();
      }
      delay(10);
      break;
  }
}
