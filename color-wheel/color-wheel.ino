// python3 ~/Library/Arduino15/packages/esp8266/hardware/esp8266/2.7.4/tools/espota.py -i 192.168.4.1 -f color-wheel.ino.generic.bin 

#include <FastLED.h>
#include "gimp_image.h"

#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

#define LED1_PIN     4
#define LED2_PIN     14
#define LED3_PIN     12
#define LED4_PIN     13

#define HALL_PIN     5

#define NUM_LEDS    27
#define BRIGHTNESS  64
#define LED_TYPE    WS2812B
#define COLOR_ORDER GRB

CRGB leds1[NUM_LEDS];
CRGB leds2[NUM_LEDS];
CRGB leds3[NUM_LEDS];
CRGB leds4[NUM_LEDS];

#define UPDATES_PER_SECOND 400

volatile unsigned long last_t;
volatile unsigned long avg_d;

void ICACHE_RAM_ATTR handleHall() {
  unsigned long t = millis();
  unsigned long d = t - last_t;
  last_t = t;
  const int k = 20;
  avg_d = (avg_d * (100 - k) + d * k) / 100; 
}

int angle() {
  unsigned long d = millis() - last_t;
  int a = 360 * d / avg_d;
  return a < 360 ? a : 360;
}

WiFiUDP Udp;

void setup() {
  WiFi.mode(WIFI_AP_STA);
  WiFi.softAP("color-wheel");
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(400);
  }

  MDNS.begin("color-wheel");
  //ArduinoOTA.setHostname("color-wheel");
  ArduinoOTA.begin();
  
  Udp.begin(4122);
    
    delay(1000 ); // power-up safety delay

    pinMode(HALL_PIN, INPUT_PULLUP);
    attachInterrupt(digitalPinToInterrupt(HALL_PIN), handleHall, FALLING); // ? FALLING / RISING

    for( int i = 0; i < NUM_LEDS; ++i) {
      leds1[i] = CRGB::Cyan;
      leds2[i] = CRGB::Yellow;
      leds3[i] = CRGB::Magenta;
      leds4[i] = CRGB::Gray;
    }

    last_t = millis();
    avg_d = 5000; // 5 sec per revolition to start?

    FastLED.addLeds<LED_TYPE, LED1_PIN, COLOR_ORDER>(leds1, NUM_LEDS).setCorrection( TypicalLEDStrip );
    FastLED.addLeds<LED_TYPE, LED2_PIN, COLOR_ORDER>(leds2, NUM_LEDS).setCorrection( TypicalLEDStrip );
    FastLED.addLeds<LED_TYPE, LED3_PIN, COLOR_ORDER>(leds3, NUM_LEDS).setCorrection( TypicalLEDStrip );
    FastLED.addLeds<LED_TYPE, LED4_PIN, COLOR_ORDER>(leds4, NUM_LEDS).setCorrection( TypicalLEDStrip );
    //FastLED.setBrightness(  BRIGHTNESS );
    
}

const long SCALE = 128;
const int ROW_SIZE = 55;
const int CX = 28;
const int CY = 28;
const int MIN_BRIGHT = 32;
const int MAX_BRIGHT = 64; //128;

inline CRGB calcp0(long s, long c, int d) {
  long y0 = c * d, x0 = s * d;
  int y = CY + y0 / SCALE, x = CX + x0 / SCALE;
  const unsigned char* p = &gimp_image.pixel_data[3 * (y * ROW_SIZE + x)];
  return CRGB(p[0], p[1], p[2]);
}

inline CRGB calcp(long s, long c, int d) {
  long y0 = CY * SCALE + c * d, x0 = CX * SCALE + s * d;
  long y = y0 / SCALE, x = x0 / SCALE;
  long py_ = y0 % SCALE, px_ = x0 % SCALE, 
      py = SCALE - py, px = SCALE - px;
  // more brightness for far pixels
  int b = MIN_BRIGHT + (MAX_BRIGHT - MIN_BRIGHT) * d / NUM_LEDS;

  const unsigned char* p =
    &gimp_image.pixel_data[3 * (y * ROW_SIZE + x)];
  int c0 = (p[0] * px + p[3] * px_) / SCALE, 
      c1 = (p[1] * px + p[4] * px_) / SCALE, 
      c2 = (p[2] * px + p[5] * px_) / SCALE;
  p += ROW_SIZE * 3;
  int c0_ = (p[0] * px + p[3] * px_) / SCALE, 
      c1_ = (p[1] * px + p[4] * px_) / SCALE, 
      c2_ = (p[2] * px + p[5] * px_) / SCALE;
  return CRGB(
    ((c0 * py + c0_ * py_) / SCALE) * b / SCALE,
    ((c1 * py + c1_ * py_) / SCALE) * b / SCALE,
    ((c2 * py + c2_ * py_) / SCALE) * b / SCALE);
}

const double angle_correction = -0.5;
const int ROWS_PER_PACKET = 3;
const int PACKET_SIZE = 3 + ROW_SIZE * ROWS_PER_PACKET * 3;
char incomingPacket[PACKET_SIZE]; 

void loop() {
    double a = -angle() * PI / 180.0 + angle_correction;
    long s = sin(a) * SCALE, c = cos(a) * SCALE;

    for( int i = 0; i < NUM_LEDS; ++i) {
      int d = NUM_LEDS - i;
      leds1[i] = calcp(s, c, d);
      leds2[i] = calcp(-c, s, d);
      leds3[i] = calcp(-s, -c, d);
      leds4[i] = calcp(c, -s, d);
    }
    
    FastLED.show();
    //FastLED.delay(1000 / UPDATES_PER_SECOND);
    //delayMicroseconds(100);
    
  int packetSize = Udp.parsePacket();
  if (packetSize) {
    int len = Udp.read(incomingPacket, PACKET_SIZE);
    if (len >= 3 && incomingPacket[0] == 0xf0) {
      unsigned startRow = incomingPacket[1], rows = incomingPacket[2];
      if (startRow + rows <= 55) {
        int offset = startRow * ROW_SIZE * 3;
        int data_len = rows * ROW_SIZE * 3;
        memcpy((char*)gimp_image.pixel_data + 3 + offset, incomingPacket + 3, data_len);
      }
    }
  }
  ArduinoOTA.handle();
  MDNS.update();   
}
