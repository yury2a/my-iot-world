from PIL import Image, ImageSequence
import socket, math, time

UDP_HOST = "192.168.4.1" # "color-wheel.local"
UDP_PORT = 4122 

ip = socket.gethostbyname(UDP_HOST)
print("UDP target IP:", ip)
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)


with Image.open("PocketCube_(small).gif") as im:
  while True:
    for frame in ImageSequence.Iterator(im):
        if frame.mode != "RGBA":
             continue
        b = frame.tobytes()

        for i0 in range(0,17):
          l = [bytes([0xf0, i0 * 3, 3])]
          for i1 in range(0,3):
            i = i0 * 3 + i1
            for j in range(0,54):
                p = 4 * (math.floor(37.5 + j * 225 / 55) + 300 * math.floor(i * 225 / 55))
                if b[p] == 0xfd and b[p + 1] == 0xfd and b[p + 2] == 0xfd:
                    l.append(bytes([0,0,0]))
                else:
                    l.append(bytes([b[p], b[p + 1], b[p + 2]]))

          msg = b''.join(l)
          sock.sendto(msg, (ip, UDP_PORT))
          time.sleep(1/8.0/18.0)
