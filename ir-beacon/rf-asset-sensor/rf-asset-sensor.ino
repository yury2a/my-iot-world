
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <EEPROM.h>
#include <RH_ASK.h>
#include <SPI.h>

RH_ASK driver(500, 7, 3);

long stopAt;

int asset_id;

long readVcc() {
  // Read 1.1V reference against AVcc
  // set the reference to Vcc and the measurement to the internal 1.1V reference
  #if defined(__AVR_ATmega32U4__) || defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
    ADMUX = _BV(REFS0) | _BV(MUX4) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  #elif defined (__AVR_ATtiny24__) || defined(__AVR_ATtiny44__) || defined(__AVR_ATtiny84__)
    ADMUX = _BV(MUX5) | _BV(MUX0);
  #elif defined (__AVR_ATtiny25__) || defined(__AVR_ATtiny45__) || defined(__AVR_ATtiny85__)
    ADMUX = _BV(MUX3) | _BV(MUX2);
  #else
    ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  #endif 

  delay(2); // Wait for Vref to settle
  ADCSRA |= _BV(ADSC); // Start conversion
  while (bit_is_set(ADCSRA,ADSC)); // measuring

  uint8_t low  = ADCL; // must read ADCL first - it then locks ADCH 
  uint8_t high = ADCH; // unlocks both

  long result = (high<<8) | low;

  result = 1125300L / result; // Calculate Vcc (in mV); 1125300 = 1.1*1023*1000
  return result; // Vcc in millivolts
}

long vcc;
int flags;

void setup() {
  pinMode(9, OUTPUT);
  digitalWrite(9, HIGH);

  pinMode(LED_BUILTIN, OUTPUT);

  vcc = readVcc();

  stopAt = millis() + 3000;
  driver.init();

  pinMode(2, INPUT);
  pinMode(4, INPUT_PULLUP);
  pinMode(5, INPUT_PULLUP);
  pinMode(6, INPUT_PULLUP);

  asset_id =
    (digitalRead(4) == LOW ? 1 : 0) |
    (digitalRead(5) == LOW ? 2 : 0) |
    (digitalRead(6) == LOW ? 4 : 0);

  flags = 0;
  if (digitalRead(2) == HIGH) flags |= 1;
  
  randomSeed(analogRead(A1) + asset_id);
}

void loop() {
  digitalWrite(LED_BUILTIN, HIGH);
  
  char msg[7];
  msg[0] = 'r'; msg[1] = 'A';
  msg[2] = '0' + asset_id;
  msg[3] = vcc / 100;
  msg[4] = flags;
  msg[5] = msg[2] ^ msg[3] ^ msg[4];
  driver.send((uint8_t *)msg, 6);
  driver.waitPacketSent();

  if (stopAt < millis()) {
    digitalWrite(9, LOW);
  }

  delay(200 + random(200));
}
