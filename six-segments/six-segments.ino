
//    4
//  2   3
//  1   5
//    0

const byte DIGITS[] = {
   0b100011,
   0b001010,
   0b011011,
   0b111001,
   0b001110,
   0b110101,
   0b101011,
   0b011010,
   0b111111,
   0b011110
};

int dd;
int pd;

void setup() {
  dd = (DDRD &= 0b00011111);
  pd = (PORTD &= 0b00011111);
}

void show(int i) {
  int p1 = i % 3;
  int p2 = (i / 3);
  if (p2 >= p1) p2++;
  
  DDRD = dd | (1 << (p1 + 5)) | (1 << (p2 + 5));
  PORTD = pd | (1 << (p1 + 5));  
}

int i = 0;
void loop() {
  int d = DIGITS[i];
  int k = 0, kk = 1;
  for (int j = 0; j < 1000; j++) {
    while ((d & kk) == 0 && kk < d) {
      k++; kk <<= 1;
    }
    show(k);
    k++; kk <<= 1;
    if (kk > d) { k = 0; kk = 1; }
    delay(1);
  }
  if (++i >= 10) i = 0;
}
