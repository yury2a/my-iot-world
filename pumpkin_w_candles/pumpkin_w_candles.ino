// Lights pumpkins with candle light via WS2812

#define FASTLED_ESP8266_RAW_PIN_ORDER
#include <FastLED.h>

#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiClient.h>
#include <simpleRPC.h>

#include "my.h"

const char* HOSTNAME = "funleds1";

const int LEDS_PIN = 5;

const int NUM_LEDS = 20;

uint16_t pos = 0;

// Seeds for choosing values from 2D noise space
uint16_t noise_coords[NUM_LEDS][2];
// Not all candles flash at the same speed.
uint16_t led_speed[NUM_LEDS];

CRGB leds[NUM_LEDS];

WiFiServer server(10040);

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);

  for (int i = 0; i < NUM_LEDS; i++) {
    int q = random16(0, 4);
    led_speed[i] = q < 2 ? q < 1 ? 2 : 3 : q < 3 ? 5 : 7;
    leds[i][0] = random16();
    leds[i][1] = random16();
  }

  WiFi.mode(WIFI_STA);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

  digitalWrite(LED_BUILTIN, LOW);
  while (WiFi.status() != WL_CONNECTED) {
    delay(100);
    digitalWrite(LED_BUILTIN, HIGH);
    delay(400);
    digitalWrite(LED_BUILTIN, LOW);
  }

  if (!MDNS.begin(HOSTNAME)) {
    delay(300);
    digitalWrite(LED_BUILTIN, HIGH);
    delay(200);
    digitalWrite(LED_BUILTIN, LOW);
  }

  digitalWrite(LED_BUILTIN, HIGH);
  server.begin();

  LEDS.addLeds<WS2812,LEDS_PIN,GRB>(leds,NUM_LEDS);
}

enum class LightMode {
  STANDARD_DISPLAY,
  STATIC,
};

LightMode lightMode = LightMode::STANDARD_DISPLAY;

long lastEffect = 0;

const CRGB RED_COLOR = CHSV(0, 255, 192);

void fillLeds() {
  for(int i = 0; i < NUM_LEDS; i++) {
    // Pick hue from RED to ORANGE.
    uint8_t hue = inoise8(leds[i][0], pos) >> 2; // HUE_RED(0) -> HUE_ORANGE(32)
    // Pick value, but take speed in account.
    long value_raw = inoise8_raw(pos * led_speed[i], leds[i][1]); // -70..70
    uint8_t value = (abs(value_raw) * value_raw) / 40 + 128; // now 0..256
    leds[i] = CHSV(hue, 255, value);
  }
}

void red() {
    lastEffect = millis();
    lightMode = LightMode::STATIC;
    for(int i = 0; i < NUM_LEDS; i++) {
      leds[i] = RED_COLOR;
    }
}

void color(uint8_t r, uint8_t g, uint8_t b) {
    lastEffect = millis();
    lightMode = LightMode::STATIC;
    for(int i = 0; i < NUM_LEDS; i++) {
      leds[i] = CRGB(r, g, b);
    }
}

void field(uint16_t *f) {
    lastEffect = millis();
    lightMode = LightMode::STATIC;
    for(int i = 0; i < NUM_LEDS; i++) {
      int c = f[i];
      leds[i] = CRGB((c >> 7) & 0xF8, (c >> 2) & 0xF8, (c << 3) & 0xF8);
    }
}

void four(uint8_t z1, uint8_t z2, uint8_t z3, uint8_t z4) {
    lastEffect = millis();
    lightMode = LightMode::STATIC;
    long f1 = (1 << z1) - 1;
    leds[0] = leds[4] = CRGB((f1 >> 16) & 255, 0, 0);
    leds[1] = leds[3] = CRGB((f1 >> 8) & 255, 0, 0);
    leds[2] = CRGB(f1 & 255, 0, 0);
    long f2 = (1 << z2) - 1;
    leds[5] = leds[9] = CRGB((f2 >> 16) & 255, 0, 0);
    leds[6] = leds[8] = CRGB((f2 >> 8) & 255, 0, 0);
    leds[7] = CRGB(f2 & 255, 0, 0);
    long f3 = (1 << z3) - 1;
    leds[10] = leds[14] = CRGB((f3 >> 16) & 255, 0, 0);
    leds[11] = leds[13] = CRGB((f3 >> 8) & 255, 0, 0);
    leds[12] = CRGB(f3 & 255, 0, 0);
    long f4 = (1 << z4) - 1;
    leds[15] = leds[19] = CRGB((f4 >> 16) & 255, 0, 0);
    leds[16] = leds[18] = CRGB((f4 >> 8) & 255, 0, 0);
    leds[17] = CRGB(f4 & 255, 0, 0);
}

void cont() {
    lightMode = LightMode::STANDARD_DISPLAY;
}

WiFiClient client;
const long RESUME_TIMEOUT = 20000;

void loop() {
  if (millis() - lastEffect >= RESUME_TIMEOUT) {
    lightMode = LightMode::STANDARD_DISPLAY;
  }

  if (!client.connected()) {
    client = server.available();
    if (client) {
      client.setNoDelay(true);
    }
  }
  if (client) {
    interface(client,
      cont, "Continue standard display",
      red, "Make red",
      color, "Display solid color (rgb)",
      field, "Just 20 16-bit colors",
      four, "Four zones");
  }

  switch (lightMode) {
    case LightMode::STATIC:
      // Static display
      break;
    case LightMode::STANDARD_DISPLAY:
    default:
      fillLeds();
      pos++;
      break;
  }

  LEDS.show();

  MDNS.update();
  delay(20);
}
