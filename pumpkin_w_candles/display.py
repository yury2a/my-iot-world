# Example how to use funleds

import pyaudio
import numpy as np
import struct
import math
import socket

FUNLEDS_HOST = "funleds1.local"

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((FUNLEDS_HOST, 10040))

CHUNK = 512
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 22050
HZ_PER_BIN = RATE / CHUNK

p = pyaudio.PyAudio()

stream = p.open(format=FORMAT,
                channels=CHANNELS,
                rate=RATE,
                input=True,
                frames_per_buffer=CHUNK)


# Eyeball (for now) frequencies, dynamic response, and scale/power
SCALE = 2.3
OFFSET = -11
DYNAMIC_RATE = 0.8

BASS = range(math.floor(60 / HZ_PER_BIN), math.ceil(130 / HZ_PER_BIN) + 1)
SNARE = range(math.floor(300 / HZ_PER_BIN), math.ceil(750 / HZ_PER_BIN) + 1)
GUITAR = range(math.floor(1500 / HZ_PER_BIN), math.ceil(4400 / HZ_PER_BIN) + 1)
KEYS = range(math.floor(6000 / HZ_PER_BIN), math.ceil(10050 / HZ_PER_BIN) + 1)


def mean(arr, r):
  sum = 0
  for i in r:
    sum += arr[i]
  return sum / len(r)


LAST = [0, 0, 0, 0]
while True:
    # Read and decode frame of 16-bit numbers
    data = stream.read(CHUNK)
    data_np = np.array(struct.unpack('{n}h'.format( n = CHUNK ), data))
    try:
      data_fft = np.absolute(np.fft.fft(data_np))

      values = [
        mean(data_fft, BASS) * 0.02,
        mean(data_fft, GUITAR),
        mean(data_fft, KEYS),
        mean(data_fft, SNARE) * 0.05,
      ]

      for i in range(4):
          # Convert magnitude to number in range 0..24
          val = np.clip(math.pow(math.log10(values[i]), SCALE) + OFFSET, 0, 24)
          # Change display values based on DYNAMIC_RATE
          LAST[i] = (1 - DYNAMIC_RATE) * LAST[i] + DYNAMIC_RATE * val
          #LAST[i] = max((1 - DYNAMIC_RATE) * LAST[i], val)

      # Send simpleRPC command for `four(LAST[0], LAST[1], LAST[2], LAST[3])`
      vals = [4] + [math.floor(i) for i in LAST]
      s.sendall(bytearray(vals))
    except:
      pass


# stream.stop_stream()
# stream.close()
# p.terminate()