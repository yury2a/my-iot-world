#include <Stepper.h>

String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete

#define STEPS_PER_MOTOR_REVOLUTION 32   
#define STEPS_PER_OUTPUT_REVOLUTION 32 * 64  //2048  

Stepper hor_stepper(STEPS_PER_MOTOR_REVOLUTION, 4, 6, 5, 7);
Stepper ver_stepper(STEPS_PER_MOTOR_REVOLUTION, 8, 10, 9, 11);

void setup() {
  // initialize serial:
  Serial.begin(9600);
  // reserve 10000 bytes for the inputString:
  inputString.reserve(10000);

  hor_stepper.setSpeed(700);
  ver_stepper.setSpeed(700);
}

void moveBy(int dx, int dy) {
  long deltaX = ((long)STEPS_PER_OUTPUT_REVOLUTION) * abs(dx) / 360;
  long deltaY = ((long)STEPS_PER_OUTPUT_REVOLUTION) * abs(dy) / 360;
  int stepX = dx < 0 ? 1 : -1;
  int stepY = dy < 0 ? -1 : 1;
  if (deltaX < deltaY) {
    int y = 0;
    for (int x = 0; x < deltaX; x++) {
      int newY = deltaY * x / deltaX;
      ver_stepper.step(stepY * (newY - y));
      y = newY;
      hor_stepper.step(stepX);      
    }
    ver_stepper.step(stepY * (deltaY - y));
  } else {
    int x = 0;
    for (int y = 0; y < deltaY; y++) {
      int newX = deltaX * y / deltaY;
      hor_stepper.step(stepX * (newX - x));
      x = newX;
      ver_stepper.step(stepY);      
    }
    hor_stepper.step(stepX * (deltaX - x));
  }
}

int parsePos;

bool parsePair(int *dx, int *dy) {
  int i = parsePos, sign = 1;
  if (inputString.charAt(i) == '-') {
    sign = -1;
    i++;
  }
  int j = i;
  while (isDigit(inputString.charAt(i))) {
    i++;
  }
  if (i == j || inputString.charAt(i) != ',') return false;
  *dx = sign * inputString.substring(j, i).toInt();
  i++;
  sign = 1;
  if (inputString.charAt(i) == '-') {
    sign = -1;
    i++;
  }
  j = i;
  while (isDigit(inputString.charAt(i))) {
    i++;
  }
  if (i == j) return false;
  *dy = sign * inputString.substring(j, i).toInt();
  parsePos = i;
  return true;
}

bool parseLine() {
  parsePos = 0;
  int dx, dy;
  bool parse = parsePair(&dx, &dy);
  if (!parse) return false;
  moveBy(dx, dy);
  while (inputString.charAt(parsePos) == ' ') {
    parsePos++;
    bool parse = parsePair(&dx, &dy);
    if (!parse) return false;
    moveBy(dx, dy);
  }
  return inputString.charAt(parsePos) == '\n';
}

void loop() {
  // print the string when a newline arrives:
  if (stringComplete) {
    if (parseLine()) {
      Serial.println("OK");
    } else {
      Serial.println("ERROR");
    }
    // clear the string:
    inputString = "";
    stringComplete = false;
  }
}

/*
  SerialEvent occurs whenever a new data comes in the
 hardware serial RX.  This routine is run between each
 time loop() runs, so using delay inside loop can delay
 response.  Multiple bytes of data may be available.
 */
void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    }
  }
}

