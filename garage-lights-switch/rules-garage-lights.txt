on power_on do
  TaskValueSet 1,1,1
  Servo 1,12,110
  Publish homeassistant/%sysname%/Power,"on"
  timerSet,1,2
endon

on power_off do
  TaskValueSet 1,1,0
  Servo 1,12,30
  Publish homeassistant/%sysname%/Power,"off"
  timerSet,1,2
endon

On Rules#Timer=1 do
  Servo 1,12,70
endon

On MQTT#Connected do
  Publish homeassistant/switch/%sysname%_pw1/config,'{"~": "homeassistant/%sysname%","name":"Garage Lights Switch", "icon": "mdi:car-outline","state_topic": "~/Power", "command_topic": "%sysname%/cmd", "state_on": "on", "state_off": "off", "payload_on": "event,power_on", "payload_off": "event,power_off"}'

  if [Power#State]=1
    Publish homeassistant/%sysname%/Power,"on"
  else
    Publish homeassistant/%sysname%/Power,"off"
  endif
endon

On MQTT#Disconnected do
endon