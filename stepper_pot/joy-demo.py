import time
import serial

ser_dev = "/dev/tty.usbserial-141320"
ser = serial.Serial(ser_dev, 9600)

time.sleep(0.1)
ser.reset_output_buffer()
ser.reset_input_buffer()

# Find first 'Pxxx' report
while True:
    l = ser.read_until()
    if l[0] == 80: break

ser.write("A\nC100\nI300,700\n".encode('utf-8'))

minD = 300
maxD = 700
mid = (718 + 278) / 2

recording = False
buffer = [(mid, 1)]
play_pos = 0

while True:
    # Fetch all responses
    distance = None
    while ser.in_waiting:
        l = ser.read_until().decode('utf-8')
        if l[:2] == 'B1':
           recording = not recording
           if recording:
              ser.write(b'M\nC30\n')
              buffer = []
              lastTime = time.time()
           else:
              ser.write(b'A\nC100\n')
              play_pos = 0
        elif l[0] == 'P':
           distance = int(l[1:-2])

    if not recording:
      t, d = buffer[play_pos]
      time.sleep(d)
      ser.write(("T" + str(t) + "\n").encode('utf-8'))
      play_pos += 1
      if play_pos >= len(buffer): play_pos = 0
    elif len(buffer) < 100000 and distance is not None:
      t = lastTime
      lastTime = time.time()
      buffer.append((distance, lastTime - t))
      time.sleep(0.2)
