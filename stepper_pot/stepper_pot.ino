// Demo of motorized potentiometer.
// See joy-demo.py for control.

#include <Stepper.h>
#include <FastLED.h>

const int LEDS_PIN = 3;

const int minPot = 278;
const int maxPot = 718;

Stepper myStepper(2038, 8, 9, 10, 11);

const int NUM_LEDS = 1;
CRGB leds[NUM_LEDS];

int target = (minPot + maxPot) / 2;

int lastVal = 0;

void setup() {
  delay(500);
  
  myStepper.setSpeed(12);

  pinMode(7, OUTPUT);
  digitalWrite(7, LOW);
  pinMode(6, INPUT_PULLUP);

  LEDS.addLeds<WS2812,LEDS_PIN,GRB>(leds,NUM_LEDS);

  leds[0] = CHSV(200, 255, 32);
  LEDS.show();

  lastVal = analogRead(A0);
  Serial.begin(9600);
}

int clamp(int v, int a, int b) {
  return v < a ? a : v > b ? b : v;
}

int clampPot(int v) {
  return clamp(v, minPot, maxPot);  
}

void mark() {
  digitalWrite(7, HIGH);
  delay(20);
  digitalWrite(7, LOW);  
}

void setTarget() {
  int val = analogRead(A0);
  int e = val - target;
  if (abs(e) > 3) {
    int d = clamp(e / 3, -5, +5);
    if (d == 0) d = e < 0 ? -1 : 1;
    myStepper.step(d);
  }  
}

int marks[10] = {300, 400, 500, 600, 700};
int marksLen = 5;

void manual() {
  int val = analogRead(A0);
  if (val != lastVal) {
    int a = val < lastVal ? val : lastVal;
    int b = val >= lastVal ? val : lastVal;
    for (int i = 0; i < marksLen; i++) {
      if (a <= marks[i] && marks[i] < b) {
        mark();
        break;
      }
    }
    lastVal = val;
  }
}

bool pressed = false;

void checkButton() {
  bool isActivated = !digitalRead(6);
  if (pressed && !isActivated) Serial.println("B0");
  if (!pressed && isActivated) Serial.println("B1");
  pressed = isActivated;
}

void setColor(int c) {
  leds[0] = CHSV(c, 255, 64);
  LEDS.show();
}

bool autoMode = false;

void readCommand() {
  int cmd = Serial.read();
  switch (cmd) {
    case 'A': autoMode = true; break;
    case 'M': autoMode = false; break;
    case 'T': target = clampPot(Serial.parseInt()); break;
    case 'I': {
      marksLen = 0;
      int i = 0;
      while (Serial.peek() != '\n' && i < 10) {
        int m = Serial.parseInt();
        marks[i++] = clampPot(m);
        marksLen++;
        if (Serial.peek() == ',') Serial.read();
      }
      break;
    }
    case 'H': {
      Serial.print('R');
      Serial.print(minPot);
      Serial.print(',');
      Serial.println(maxPot);
      break;
    }
    case 'C': setColor(Serial.parseInt()); break;
  }
  if (Serial.peek() == '\n') Serial.read();
}

void loop() {
  if (Serial.available() > 0) {
    readCommand();
  }

  checkButton();

  if (autoMode) {
    setTarget();
  } else {
    manual();
  }
  
  int val = analogRead(A0);
  Serial.print("P");
  Serial.println(val);
  delay(20);
}
