
void setup() {
  AC0_CTRLA = AC_ENABLE_bm | AC_HYSMODE_1_bm | AC_HYSMODE_0_bm;
  AC0_MUXCTRLA = 0;
  AC0_INTCTRL = AC_CMP_bm;

  pinMode(PIN_PB0, OUTPUT);
  pinMode(PIN_PB1, OUTPUT);
  pinMode(PIN_PB2, OUTPUT);
  pinMode(PIN_PB3, OUTPUT);
  pinMode(PIN_PA3, INPUT_PULLUP);

  init_decoder();
  reset_buffer();
}


uint8_t buf[31];

void reset_buffer() {
  buf[0] = 4;
  buf[1] = 8; buf[2] = 4; buf[3] = 2; buf[4] = 1;
}

volatile bool preamble = false;
volatile bool phase;

uint32_t last_t;
int16_t ds[4];
uint8_t di = 0;
int c = 0;

void init_decoder() {
  preamble = false;
  memset(ds, 0, sizeof ds);

  digitalWrite(PIN_PB2, HIGH);

}

const int accept_diff = 10;
const int min_duration = accept_diff * 5;
const int max_duration = 500;
const uint8_t preamb_detected = 8;


ISR (AC0_AC_vect) {
  phase = (AC0_STATUS & AC_STATE_bm);

  uint32_t t = millis();
  ds[di] = t - last_t;
  di = (di + 1) & 3;
  last_t = t;

  int sum = abs(ds[0] - ds[1]) + abs(ds[2] - ds[1]) + abs(ds[2] - ds[3]) + abs(ds[0] - ds[3]);
  preamble = ds[di] >= min_duration && ds[di] <= max_duration && sum <= 4 * accept_diff;

  AC0_STATUS = AC_CMP_bm;
}

void receive() {
  cli();
  uint32_t t = last_t;
  int16_t d = (ds[0] + ds[1]) * 3 / 8; // 3/4 of d
  sei();
  digitalWrite(PIN_PB0, HIGH);
  digitalWrite(PIN_PB2, LOW);
  digitalWrite(PIN_PB3, LOW);
  bool p, p0;
  uint32_t next = t + d;
  while(next <= millis()) next += d;
  next -= millis();
  do {
    p = phase;
    delay(next);

    p0 = phase;
    t = millis();
    while (p0 == phase) {
      if (long(millis() - t) > d) {
        digitalWrite(PIN_PB3, HIGH);
        return;
      }
    }

    next = d;
  } while (p == p0);
  digitalWrite(PIN_PB1, HIGH);
  int k = 0; int km = 1;
  int len = 1;
  while (k < len) {
    delay(d);

    p0 = phase;
    t = millis();
    while (p0 == phase) {
      if (long(millis() - t) > d) {
        digitalWrite(PIN_PB3, HIGH);
        return;
      }
    }

    if (p0 != p) {
      buf[k] |= km;
    } else {
      buf[k] &= ~km;
    }
    km <<= 1;
    if (km > 128) {
      if (k == 0) len = min(30, buf[0]) + 1;
      k++; km = 1;
    }
  }
  digitalWrite(PIN_PB2, HIGH);
}

void display(int n) {
  digitalWrite(PIN_PB0, !!(n & 1));
  digitalWrite(PIN_PB1, !!(n & 2));
  digitalWrite(PIN_PB2, !!(n & 4));
  digitalWrite(PIN_PB3, !!(n & 8));
}

int p = 0;

void loop() {
  if (p >= min(30, buf[0])) {
    p = 0;
  }
  display(buf[p + 1]);
  p++;
  
  for (int i = 0; i < 333; i++) {
    if (preamble) {
      receive();
      init_decoder();
      digitalWrite(PIN_PB0, LOW);
      digitalWrite(PIN_PB1, LOW);
      break;
    }
    if (!digitalRead(PIN_PA3)) {
      reset_buffer();
      break;
    }
    delay(1);
  }
}


