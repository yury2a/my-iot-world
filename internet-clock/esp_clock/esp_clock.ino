#include "buttons.h"
#include "wifi.h"

void setup() {
  buttons::setup();
  wifi::setup();
  //pinMode(LED_BUILTIN, OUTPUT);

  //buttons::set_time(17, 42);
}

bool dirty = true;

void loop() {
  wifi::loop();

  if (dirty) {
    int h, m, s,mo, d;
    if (wifi::get_time(&h, &m, &s, &mo, &d)) {
      if (s >= 57) {
          delay((60 - s) * 1000 - 200);
          m++;
          if (m >= 60) {
            m -= 60; h++;
          }
          buttons::set_time(h, m);
      } else if (s != 0) {
          unsigned long t = millis() + (60 - s) * 1000 - 200;
          buttons::set_time(h, m);
          if (s < 40) {
            delay(25000);
            buttons::reset_sec();
            if (s < 20) {
              delay(15000);
              buttons::reset_sec();                        
            }
          }
          delay(t - millis());
          buttons::reset_sec();            
          buttons::press_min(1);
          m++;
          if (m >= 60) {
            buttons::press_hour(1);
            m -= 60; h++;
          }
      } else {
         buttons::set_time(h, m);
      }
      dirty = false;
    }
  }
}
