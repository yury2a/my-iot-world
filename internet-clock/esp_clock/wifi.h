#pragma once

#include <NTPClient.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

namespace wifi {
 
const char *ssid     = "<WIFI>";
const char *password = "<WIFI_PWD>";

WiFiUDP ntpUDP;

NTPClient timeClient(ntpUDP);

void setup(){
  WiFi.begin(ssid, password);

  while ( WiFi.status() != WL_CONNECTED ) {
    delay ( 500 );
  }

  timeClient.setUpdateInterval(4 * 60 * 60 * 1000);
  timeClient.begin();
}

void civil_from_days(unsigned long z, int *py, int *pm, int *pd, int *pw)
{
  struct tm * ti;
  time_t rawtime = z;
  ti = localtime (&rawtime);
  *py = ti->tm_year + 1900;
  *pm = (ti->tm_mon + 1) < 10 ? 0 + (ti->tm_mon + 1) : (ti->tm_mon + 1);
  *pd = (ti->tm_mday) < 10 ? 0 + (ti->tm_mday) : (ti->tm_mday);
  *pw = ti->tm_wday;
}

unsigned weekday_from_days(unsigned long z)
{
    return ((unsigned)(z % 7) + 4) % 7;
}

const unsigned long CST_OFFSET = -6 * 3600L;
const unsigned long CDT_DELTA = 3600L;

bool is_cdt(int m, int d, int wd, int h) {  
  if (3 < m && m < 11) return true;
  if (m < 3 || (m == 3 && d < 8) ||
      m > 11 || (m == 11 && d >= 8)) return false;

  if (m == 3) {
    int s = ((6 + d - wd) % 7) + 9;
    return s < d || (d == s && h < 2);
  } else {
    int s = ((6 + d - wd) % 7) + 1;
    return s > d && !(d == s && h < 2);
  }
}

bool get_time(int *h, int *m, int *s, int *dm, int *dd) {
  long unsigned z = timeClient.getEpochTime();
  struct tm * ti;
  time_t rawtime = z;
  ti = localtime (&rawtime);
  z += CST_OFFSET;
  
  int y, mo, d, wd;
  civil_from_days(z, &y, &mo, &d, &wd);
  if(y < 2007) return false;

  if (is_cdt(mo, d, wd, (z % 86400L) / 3600)) z += CDT_DELTA;
  
  *h = (z % 86400L) / 3600;
  *m = (z % 3600) / 60;
  *s = z % 60;
  *dm = mo;
  *dd = d;
  return true;
}

void loop() {
  timeClient.update();
}

};
