#pragma once

namespace buttons {

void alarm(bool f) {
  digitalWrite(3, f ? HIGH : LOW);
}
void sleep(bool f) {
  digitalWrite(14, f ? HIGH : LOW);
}
void hour(bool f) {
  digitalWrite(4, f ? HIGH : LOW);
}
void minute(bool f) {
  digitalWrite(16, f ? HIGH : LOW);
}

void rel() {
  minute(false);
  hour(false);
  delay(25);
  sleep(false);
  delay(10);
  alarm(false);
}

void press_min(int n) {
  while(n > 0) {
    n--;
    minute(true);
    delay(25);
    minute(false);
    delay(35);
  }
}

void press_hour(int n) {
  while(n > 0) {
    n--;
    hour(true);
    delay(25);
    hour(false);
    delay(35);
  }
}

void reset_time() {
  alarm(true);
  sleep(true);
  minute(true);
  hour(true);
  delay(30);
  rel();
  delay(40);
}

void reset_sec() {
  alarm(true);
  sleep(true);
  delay(20);
  hour(true);
  delay(20);
  rel();
  delay(40);
}

void setup() {
  pinMode(16, OUTPUT);
  digitalWrite(16, LOW);
  pinMode(4, OUTPUT);
  digitalWrite(4, LOW);
  pinMode(3, OUTPUT);
  digitalWrite(3, LOW);
  pinMode(14, OUTPUT);
  digitalWrite(14, LOW);
}

void set_time(int h, int m){
  reset_time();
  press_min(m);
  press_hour(h); 
}

};
